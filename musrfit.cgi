#!/usr/bin/perl
#
# Copyright Zaher Salman 2009-2018.
# zaher.salman@psi.ch

$ENV{PERL5LIB}=".";

# Relevant subdirectories and urls
# May need modification on different servers
$WEBDIR   = "/tmp";
$REALDIR  = "/var/www/html/tmp";
$BINDIR   = "/usr/local/bin";
$MAINPAGE = "/cgi-bin/musrfit.cgi";

$OUTPUT      = $WEBDIR;

$SUMM_DIR = "/mnt/data/nemu/summ";

# Keep log file of all calls
$MYLOG = "/var/www/html/tmp/mulog.keep";

# Defined for clarity
$EMPTY=q{};
$SPACE=q{ };
$COMMA=q{,};

# Additions to paremeters' names
$erradd = "d";
$minadd = "_min";
$maxadd = "_max";

%BeamLines = ( "LEM", "MUE4", "LEM (PPC)", "MUE4","GPS", "PIM3", "FLAME", "PIM3", "LTF", "PIM3", "Dolly", "PIE1" , "GPD", "PIE1", "HAL", "PIE3");

use CGI;
use lib "/var/www/cgi-bin";
use MSR;

my $in = new CGI;

# Common to all steps
# If you are coming from a previous round pass in various parameters
PassIn();

# Programs used often
# Fit the data
if ( $TITLE eq $EMPTY || $TITLE eq "TITLE" ) {
    # with title from data file (-t flag)
    $MUSRFIT = $BINDIR . "/musrfit -t -e --timeout 300";
} else {
    # with manually set title
    $MUSRFIT = $BINDIR . "/musrfit -e --timeout 300";
}
# Create plot png
$MUSRVIEW  = $BINDIR . "/musrview --timeout 300 --png";
# Create ascii files
$DUMPASCII = $BINDIR . "/musrview --timeout 300 --ascii";
$MSR2DATA = $BINDIR . "/msr2data";

# Commands used often for cleaning up
$cln     = "cd $REALDIR; rm -f MINUIT2.* FILENAME* ";

if ($fftv ne "y") {
    # Plot asymmetry in the time domain
    $plotonly =
	"cd $REALDIR; $MUSRVIEW FILENAME.msr >> FILENAME.out 2>&1; convert FILENAME\_0.png -crop 590x460+1+50 FILENAME.png; rm -f FILENAME\_0.png";
    $fitplot =
	"cd $REALDIR;  rm -f FILENAME.png ; $MUSRFIT FILENAME.msr > FILENAME.out 2>&1; $MUSRVIEW FILENAME.msr >> FILENAME.out 2>&1; convert FILENAME\_0.png -crop 590x460+1+50 FILENAME.png; rm -f FILENAME\_0.png";
    $dumpascii =
	"cd $REALDIR; rm -f FILENAME.dat*; $DUMPASCII FILENAME.msr > FILENAME_ascii.out 2>&1; gzip FILENAME.dat";
} else {
    # plot FFT
    $plotonly =
	"cd $REALDIR; rm -f FILENAME.png ; $MUSRVIEW -f FILENAME.msr >> FILENAME.out 2>&1; convert FILENAME\_0\_F.png -crop 590x460+1+50 FILENAME.png; rm -f FILENAME\_0\_F.png";
    $fitplot =
	"cd $REALDIR; rm -f FILENAME.png ; $MUSRFIT FILENAME.msr > FILENAME.out 2>&1; $MUSRVIEW -f FILENAME.msr >> FILENAME.out 2>&1; convert FILENAME\_0\_F.png -crop 590x460+1+50 FILENAME.png; rm -f FILENAME\_0\_F.png";
    $dumpascii =
	"cd $REALDIR; rm -f FILENAME.dat*; $DUMPASCII -f FILENAME.msr > FILENAME_ascii.out 2>&1; gzip FILENAME.dat";    
}

# This is the content of the html page..
$Content = $EMPTY;

# This is just for temporary debugging html text
$TC = $EMPTY;

# See which step or command we are coming from
# Step Value - Command
#
# Restart    - CLEAR/START NEW
# SHARE      - Select/Define Shared
# PLOT       - Plot without minimization
# FIT        - Fit and plot the results
#######################################################################
$Step = $in->param("go");
#$TC=~ s/\n/<br>/g;

# Put all input values in a hash
@All = $in->param();
foreach $One (@All) {
    $All{ $One } = $in->param($One);
    #    $TC=$TC."$One=".$All{ $One }."<br>";
}

# First create the THEORY block to get a parameters list
($Full_T_Block,$Paramcomp_ref)=MSR::CreateTheory(@FitTypes);
@Paramcomp = @$Paramcomp_ref;
# If there is not Func_T_Block initialize it
if ($Func_T_Block eq $EMPTY) {$Func_T_Block=$Full_T_Block;}

if ( $Step eq "SHARE" && $#RUNS != 0 ) {
    # Choose shared parameters only if you have multiple runs
    StepShared();
} elsif ($Step eq "FIT" || $Step eq "PLOT" || ( $Step eq "SHARE" && $#RUNS == 0 )) {
    # Create msr input file
    if ($FitAsyType eq "Asymmetry") {
	($Full_T_Block,$Paramcomp_ref,$FullMSRFile)= MSR::CreateMSRUni(\%All);
	# Open output file FILENAME.msr
	open( OUTF,q{>},"$REALDIR/$FILENAME.msr" );
	print OUTF ("$FullMSRFile");
	close(OUTF);
    } else {
	($Full_T_Block,$Paramcomp_ref,$FullMSRFile)= MSR::CreateMSRGLB(\%All);
	# Open output file FILENAME.msr
	# conver RUNS[0] to number and use it in file names (remove leading 0s).
	$RUNS[0] = 1*$RUNS[0];
	open( OUTF,q{>},"$REALDIR/$RUNS[0]_tmpl.msr" );
	print OUTF ("$FullMSRFile");
	close(OUTF);

	# Change runs line in the final global fit
	# For single hist fits
	$NSpectra = ($#RUNS+1)*($#Hists+1);
	# but for Asymmetry fits
        if ($FitAsyType eq "AsymmetryGLB") {$NSpectra = ($#RUNS+1);}
        $NewRunLine = "runs ".join(" ",(1...$NSpectra));

        $RunList = join(" ",@RUNS);
        # Only for multiple runs
        if ($#RUNS != 0) {
            # Use msr2data to generate global fit MSR file
            $cmd =  "cd $REALDIR; ".$MSR2DATA. " \[".$RunList."\] "." _tmpl msr-".$RUNS[0]." global >> debug.txt 2>&1";
            # create the global file 
            $tmp=system("$cmd");
            $cmd =  "msr2data \[".$RunList."\] "." _tmpl msr-".$RUNS[0]." global";
            $TC=$TC."<br>Command line: ".$cmd;
            # change the stupid name
            $StupidName = $RUNS[0]."+global_tmpl.msr";
            # change stupid default runs line
	    $cmd = "cd $REALDIR; cp $StupidName ".$FILENAME.".msr; perl -pi -e 's/runs.*?(?=\n)/$NewRunLine/s' $FILENAME.msr >> debug.txt 2>&1";
	    #$TC=$TC."<br>".$cmd;
            $tmp=system($cmd);
        } else {
            # Single run, copy template to pattern file name
            $cmd =  "cd $REALDIR; cp $RUNS[0]_tmpl.msr $FILENAME.msr >> debug.txt 2>&1";
            $tmp=system($cmd);
        }
        # feed in values of parameters if they exist
	$wholefile = $EMPTY;
	$newline = $EMPY;
	{open (MSRF,q{<},"$REALDIR/$FILENAME.msr" );
	 local $/ = undef;
	 $wholefile = <MSRF>;
	 close(MSRF);}
	my %PTable=MSR::PrepParamTable(\%All);
	my $NParam=keys( %PTable );
	for (my $iP=0;$iP<$NParam;$iP++) {
	    my ($Param,$value,$error,$minvalue,$maxvalue,$RUNtmp) = split(/,/,$PTable{$iP});
	    if (defined($All{"$Param"})) {
		# replace the corresponding $Param  line
		if  ($All{"$Param$minadd"} == $All{"$Param$maxadd"} ) {
		    $All{"$Param$minadd"} = "";
		    $All{"$Param$maxadd"} = "";
		}
		$newline = join("    ",($Param,$All{"$Param"},$All{"$erradd$Param"},$All{"$erradd$Param"},$All{"$Param$minadd"},$All{"$Param$maxadd"}));
		$wholefile =~ s/$Param.*/$newline/;
		#$TC = $TC."Replacing: ".$PTable{$iP}."<br>By: $newline<br>";
	    }
	}
	# Now rewrite the msr file
	open (MSRF,q{>},"$REALDIR/$FILENAME.msr" );
	print MSRF $wholefile;
	close(MSRF);
    }
    # fit and plot or plot only?
    $cmd = $plotonly;
    if ( $Step eq "FIT") {
        $cmd = $fitplot;
    }

    # Fit and/or plot results
    # Generate ascii data and fit
    $cmd = join(";",$cmd,$dumpascii);
    $cmd =~ s/FILENAME/$FILENAME/g;
    $tmp = system("$cmd");

    # Generate parameters table with a header
    $All{"Header"}=1;
    if ($FitAsyType eq "Asymmetry") {    
	# For old style
	$TABLE=MSR::ExportParams(\%All);
	# Open output parameters data file E.msr
	open( OUTF,q{>},"$REALDIR/$FILENAME\_par.dat" );
	print OUTF ("$TABLE");
        close(OUTF);
    } else {
        # use msr2data
        if ($#RUNS != 0) {
            $cmd =  "cd $REALDIR; cp $FILENAME.msr $StupidName;".$MSR2DATA. " \[".$RunList."\] "." _tmpl new global data -o $FILENAME\_par.dat >> debug.txt 2>&1";
            #$TC=$TC."<br>Fit command: ".$cmd;
            $tmp = system("$cmd");
            $cmd =  "msr2data \[".$RunList."\] "." _tmpl new global data -o $FILENAME\_par.dat";
        } else {
            $cmd =  "cd $REALDIR; cp $FILENAME.msr $RUNS[0]_tmpl.msr;".$MSR2DATA. " ".$RunList." "." _tmpl new data -o $FILENAME\_par.dat >> debug.txt 2>&1";
            #$TC=$TC."<br>Fit command: ".$cmd;
            $tmp = system("$cmd");
            $cmd =  "msr2data ".$RunList." "." _tmpl new data -o $FILENAME\_par.dat";
        }           
        $TC=$TC."<br>Extract parameters: ".$cmd;
    }
    
    $Spectra = ( $#RUNS + 1 ) * ( $#TiVals + 1 );
    if ( $RRF != 0 && $Spectra == 1 ) { $Spectra = 2; }

    PrintOut();
} else {
    if ( $Step eq "Restart" ) {
        # Clean up
        $cmd = $cln;
        $cmd =~ s/FILENAME/$FILENAME/g;
        $tmp = system("$cmd");
        $Step    = 0;
        $Content = $EMPTY;
	# Do not pass FUNCTIONS related value
	undef $Func_T_Block;
	undef $FunctionsBlock;
    }
    # Show start page
    Step0();
}

# Reset the values of the TITLE and FILENAME if default was chosen
if ($T_Keep) {$TITLE = $EMPTY;}
if ($FN_Keep) {$FILENAME = $EMPTY;}

# Insert passed on variables into the HTML code
PassOn();
SpitHTML();
exit(0);

######## Split functions and subroutines ########

########################
#sub SpitHTML
#
#This function prints the start of the feedback form web page.
########################
sub SpitHTML {
    print("Content-type: text/html; charset=iso-8859-1  \n\n");
    print(
        "<HTML>
<HEAD>
<TITLE>$TITLE</TITLE>
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; no-cache; charset=iso-8859-8-1\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.7\">
</HEAD>
$Content
$TC
<BR><HR>
If you find any bugs please send me an <a href=\"mailto:zaher.salman\@psi.ch\">email</a>.<br>
</BODY>
</HTML>
"
    );
    return;
}
########################

########################
# Step 0
#
# Is the begining of the fitting pocess
# Take title and run numbers and the type of fitting function
########################
sub Step0 {

    $RUNSType  = $in->param("RUNSType");
    if ($RUNSType) {
	$RunName = "RunFiles";
    } else {
	$RunName = "RunNumbers";
    }
    $RunNameValue = $in->param($RunName);

    $LRBF = $in->param("LRBF");
    $FN_Keep = $in->param("FN_Keep");
    if ($FN_Keep || $FN_Keep eq $EMPTY ) {$FILENAME = $EMPTY;}
    $T_Keep = $in->param("T_Keep");
    if ($T_Keep || $T_Keep eq $EMPTY) {$TITLE = $EMPTY;}

    $Components = "";

    if ($All{"numComps"} eq "") {$All{"numComps"}=1;}
    # Prepare body of the HTML code
    $Content = qq{
<BODY BGCOLOR=\"$BGCOLOR\" BACKGROUND=\"$BACKGROUND_URL\" TEXT=\"$TEXT_COLOR_NORMAL\" onload=\"adjust_comps();default_histograms();\">
<script>
function expandCollapse() {
   for (var i=0; i<expandCollapse.arguments.length; i++) {
      var element = document.getElementById (expandCollapse.arguments[i]);
      element.style.display = (element.style.display == "none") ? "block" : "none";
   }
}

function default_histograms(force)
{
    let beamLines = ["LEM", "LEM (PPC)", "GPS", "LTF", "FLAME", "Dolly", "GPD", "HAL"];
        defltHists = ["1 5,3 7", "21 25,23 27", "2,1", "2,1", "2,1", "1,2" , "1,2", "4 5 6,8 9 2"];
        histInfo = ["Left - 1 5, Right - 3 7, Up - 2 6, Down - 4 8",
                    "Left - 21 25, Right - 23 27, Up - 22 26, Down - 24 28",
                    "Forw - 1, Back - 2, Up - 3, Down - 4, Right - 5, Left - 6",
                    "",
                    "Forw - 1, Back - 2, Right - 3, Left - 4",
                    "Forw - 1, Back - 2, Left - 3, Right - 4",
                    "",
                    ""];
        beamLine = document.getElementById('beam_menu').value;
    let init_LRBF = '$LRBF';
    console.log('Initial LRBF='+init_LRBF);
    // If LRBF is empty or you get a force=1 put default valuer of beamline 
    if (force == 1 || init_LRBF == '') {
	// get value in HTML
	let asyLRBF = document.getElementById('LRBF');
        // which beamline is selected
        let iofBeamLine = beamLines.findIndex(element => element.includes(beamLine));
        asyLRBF.value = defltHists[iofBeamLine];
        console.log('Setting value='+defltHists[iofBeamLine]+' index='+iofBeamLine+' Looking for beamline='+beamLine);
        let infoDiv = document.getElementById('histInfo');
        console.log(infoDiv)
        // in the future exctact from a file header
        if (infoDiv && histInfo[iofBeamLine] != "") {
           infoDiv.innerText = beamLine + ": " + histInfo[iofBeamLine];
        } else {
           infoDiv.innerText = "";
        }
   }
}

function fill_years()
{
    let max = new Date().getFullYear();
        min = 1994;
        select = document.getElementById('year_menu');

    for (let i = max; i>=min; i--){
       let opt = document.createElement('option');
       opt.value = i;
       opt.innerHTML = i;
       select.appendChild(opt);
    }
    let selYear = "$YEAR";
    if (selYear == "") {
	select.value = max;
    } else {
	select.value = selYear;
    }
}

function adjust_comps()
{
   var numComps = document.getElementById("numComps").value;
   var CTable = document.getElementById("CTable");
   var Nrows = CTable.rows.length;
   var FitTypes = [];
   var FuncsDict = {};
   FuncsDict["None"]="None";
   FuncsDict["Background"]="Constant Background";
   FuncsDict["Exponential"]="Exponential";
   FuncsDict["Gaussian"]="Gaussian";
   FuncsDict["Stretch"]="Stretch Exponential";
   FuncsDict["ExponentialCos"]="Exponential Cosine";
   FuncsDict["GaussianCos"]="Gaussian Cosine";
   FuncsDict["StretchCos"]="Stretch Cosine";
   FuncsDict["AbragamCos"]="Abragam Cosine";
   FuncsDict["GBGCos"]="Gaussian Brodened Gaussian Cosine";
   FuncsDict["ExponentialBessel"]="Exponential Bessel";
   FuncsDict["GaussianBessel"]="Gaussian Bessel";
   FuncsDict["SLKT"]="Static Lorentzian KT";
   FuncsDict["SGKT"]="Static Gaussian KT";
   FuncsDict["LGKT"]="Combi Lorentzian/Gaussian KT";
   FuncsDict["STRKT"]="Stretched KT";
   FuncsDict["GBGZF"]="Gaussian Brodened Gaussian ZF";
   FuncsDict["LDKTLF"]="Lorentzian Dynamic KT LF";
   FuncsDict["GDKTLF"]="Gaussian Dynamic KT LF";
   FuncsDict["LLFExp"]="Lorentzian KT LF x Exp";
   FuncsDict["GLFExp"]="Gaussian KT LF x Exp";
   FuncsDict["LGKTExp"]="Combi Lorentzian/Gaussian KT x Exp";
   FuncsDict["GBGExp"]="Gaussian Brodened Gaussian ZF x Exp";
   FuncsDict["STRKTExp"]="Stretched KT x Exp";
   FuncsDict["STRKTSExp"]="Stretched KT x Str Exp";
   FuncsDict["LLFSExp"]="Lorentzian KT LF x Str Exp";
   FuncsDict["GLFSExp"]="Gaussian KT LF x Str Exp";
   FuncsDict["LGKTSExp"]="Combi Lorentzian/Gaussian KT x Str Exp";
   FuncsDict["GBGSExp"]="Gaussian Brodened Gaussian ZF x Str Exp";

   for (i=1;i<=numComps;i++) {
       var FitType = 'FitType'+i;
       var SetectType = document.getElementById(FitType);
       if (i == 1) {
	   FitTypes[i-1] = 'ExponentialCos';
       } else {
	   FitTypes[i-1] = 'None';
       }
       // Is the component there?
       if (SetectType != null) {
	   // There is a selection, use it;
	   FitTypes[i-1] = SetectType.value;
       }
       console.log('FitType ='+FitType+' with '+FitTypes[i-1]);
   }

	   console.log('Add rows, rows='+Nrows+' comps='+numComps);
   if (Nrows == 0 && numComps == 1) {
       console.log('Creating component 1');
       FitType = 'FitType1';
       SetectType = document.getElementById(FitType);
       // Creat this component
       var row = CTable.insertRow(0);
       var Li = row.insertCell(0);
       Li.innerHTML = 'Component #1';
       var Comp = row.insertCell(1);

       var compCell = document.createElement("select");
       compCell.name = "FitType1";
       compCell.name = "FitType1";
       Comp.appendChild(compCell);
       for (var Func in FuncsDict) {
           var el = document.createElement("option");
           el.textContent = FuncsDict[Func];
           el.value = Func;
           compCell.appendChild(el);
       }
       // now set its initial value
       compCell.value = FitTypes[0];       
   } else if (numComps >= Nrows) {
       for (i=Nrows+1;i<=numComps;i++) {
	   console.log('.. Creating component '+i);
	   FitType = 'FitType'+i;
	   SetectType = document.getElementById(FitType);
	   // Creat this component
	   var row = CTable.insertRow(i-1);
	   var Li = row.insertCell(0);
	   Li.innerHTML = 'Component #'+(i);
	   var Comp = row.insertCell(1);

	   var compCell = document.createElement("select");
	   compCell.name = "FitType"+i;
	   compCell.name = "FitType"+i;
	   Comp.appendChild(compCell);
           for (var Func in FuncsDict) {
	       var el = document.createElement("option");
	       el.textContent = FuncsDict[Func];
	       el.value = Func;
	       compCell.appendChild(el);
	   }
	   // now set its initial value
	   compCell.value = FitTypes[i-1];
       }
   } else {
       // Delete extra components
       console.log('Remove rows, rows='+Nrows+' comps='+numComps);
       for (i = Nrows-1;i>=numComps;i--) {
	   CTable.deleteRow(i);
       }
   }
}
</script>
<form accept-charset=utf-8 enctype="multipart/form-data" action="$MAINPAGE" method="post">
<table cellpadding="2" cellspacing="10" border="0" width="610">
<tbody>
<tr>
<td colspan="2">RUN numbers, comma separated</td><td colspan="2"><input type="text" name="$RunName" value="$RunNameValue" size="27"></td>
</tr>
<tr>
<td colspan="2">On beam line 
<select name="BeamLine" id="beam_menu" style="margin:0;align:left" onchange="default_histograms(1)">
INSERT_BEAMLINES_HERE
</select></td>
<td colspan="2">Year  
<select id="year_menu" name="YEAR">
<div align="left">
<script>fill_years()</script>
</div>
</select></td>
</tbody>
</table>
<br>
Fitting function:<br>
<br>
<label>Number of components: </label><input name="numComps" id="numComps" type="number" step="1" min="1" max="100" value="$All{"numComps"}" onchange="adjust_comps()">
<br><br>                             
<table id="CTable" cellpadding="2" cellspacing="0" border="1" width="610">
<tbody>
</tbody>
</table>
<br>
<br>
<table cellpadding="2" cellspacing="0" border="1" width="610">
<tbody>
<tr>
<td>
From t<sub>i</sub>=<br>\[&mu;sec\] <input type="text" name="Tis" size="4" value="$Tis">
</td>
<td>
To t<sub>f</sub>=<br>\[&mu;sec\] <input type="text" name="Tfs" size="4" value="$Tfs">
</td>
<td>
Binning factor=<br>[points] <input type="text" name="BINS" size="4" value="$BINS">
</td>
</tr>
<tr>
<td colspan="3">
Fit type <select name="FitAsyType">
<option $FitAsyType{"Asymmetry"} value="Asymmetry">Asymmetry</option>
<option $FitAsyType{"SingleHist"} value="SingleHist">Single Hist</option>
<option $FitAsyType{"AsymmetryGLB"} value="AsymmetryGLB">Asymmetry GLB</option>
<!option $FitAsyType{"RotRF"} value="RotRF">Rotating Reference Frame</option>
</select> 
Histograms: <input type="text" name="LRBF" id="LRBF" size="16" value="$LRBF">
</td>
</tr>
</tbody>
</table>
<div style="display: table; margin-top:10px; background-color: yellow;" id="histInfo"></div>    
<!RRF Frequency in MHz (Default 0, no RRF) = <input type="text" name="RRF" value="$RRF">
<br>
<div id="TiExtraStuff" style="">
<a href="javascript:expandCollapse('ExtraStuff','none');">Extra option</a>
</div>
<div id="ExtraStuff" style="display: none;">
<table cellpadding="2" cellspacing="10" border="0" width="610">
<tbody>
<tr>
<td colspan="4">Enter a label (default is run title)<br></td>
</tr>
<tr><td colspan="4">
<input type="text" name="TITLE" value="$TITLE" size="63" >
<input type="hidden" name="T_Keep" value="$T_Keep">
<input type="hidden" name="fftv" value="$fftv">
<input type="hidden" name="logx" value="$logx">
<input type="hidden" name="logy" value="$logy">
<input type="hidden" name="ltc" value="$ltc">
</td></tr>
<tr>
<td colspan="2">Enter a name for output file (optional) </td>
<td colspan="2">
<input type="text" name="FILENAME" value="$FILENAME" size="27">
<input type="hidden" name="FN_Keep" value="$FN_Keep">
</td>
</tr>
</tbody>
</table>
</div>
<br>
PASS_ON_HTML_CODE
<input type="hidden" name="go" value="SHARE"><br>
<input type="submit" value="Next>"></form>
};

    for my $BeamLine ( keys %BeamLines ) {
        $BeamLineDDM = $BeamLineDDM
          . "<option $BeamSel{\"$BeamLine\"} value=\"$BeamLine\">$BeamLine</option>\n";
    }

    # Insert the beamlines into the HTML code
    $Content =~ s/INSERT_BEAMLINES_HERE/$BeamLineDDM/g;
    return;
}
########################

########################
# StepShared
#
# Choose shared parameters if applicable
#
########################
sub StepShared {

    # Here we need first a list of parameters which depends
    # on the functions chosen and number of components

    $Content = $Content . "
<BODY BGCOLOR=\"$BGCOLOR\" BACKGROUND=\"$BACKGROUND_URL\" TEXT=\"$TEXT_COLOR_NORMAL\" onload=\"\">
Select the shared parameters<br>
<form accept-charset=utf-8 enctype=\"multipart/form-data\" action=\"$MAINPAGE\"
 method=\"post\"><br>
INSERT_PARAMETERS_TABLE_HERE
PASS_ON_HTML_CODE
<input type=\"hidden\" name=\"go\" value=\"PLOT\"><br>
<input type=\"submit\" value=\"Next>\">
</form>
";

    # Prepare table of parameters
    $component   = 0;
    $Param_Table = $EMPTY;
    foreach my $FitType (@FitTypes) {
        ++$component;
        $Parameters  = $Paramcomp[ $component - 1 ];
        $Param_Table = $Param_Table . "
Component $component: $FitType
<table cellpadding=\"2\" cellspacing=\"2\" border=\"1\" width=\"20%\">
<tbody>
<tr>
<td style=\"vertical-align: top; text-align: center;\"><b><i>Parameter</i></b><br>
</td>
<td style=\"vertical-align: top; text-align: center;\"><b><i>Share</i></b><br>
</td>
</tr>";

        @Params = split( /\s+/, $Parameters );

        # For the first component we need Alpha for Asymmetry fits
        if ( $component == 1 && $FitAsyType eq "Asymmetry" ) {
            unshift( @Params, "Alpha" );
        } elsif ( $component == 1 && $FitAsyType eq "AsymmetryGLB" ) {
            unshift( @Params, "Alpha" );
        } elsif ( $component == 1 && $FitAsyType eq "SingleHist" ) {
	    if (grep(/Phi/,@Params)) {
		# remove the Phi and put it back for each histogram
		@Params = grep ! /Phi/,@Params;
		# for each histogram
		foreach my $Hist (@Hists) {
		    # take only first histogram from sum
		    ($Hist,$tmp) = split(/ /,$Hist);
		    # Doesn't make sense to share No or NBg!
		    # unshift( @Params, ( "N0$Hist", "NBg$Hist", "Phi$Hist" ) );
		    push(@Params,"Phi$Hist");
		}
	    }
        }

        foreach my $Param (@Params) {
            if ( ($#FitTypes != 0) && ( $Param ne "Alpha" && $Param ne "N0" && $Param ne "NBg" ) ) {
                $Param = join( $EMPTY, $Param, "_", "$component");
            }
            $ischecked = $in->param("Sh_$Param");
            if ($ischecked) {
                $checked = "checked";
            } else {
                $checked = $EMPTY;
            }
            $Param_Table = $Param_Table . "
<tr>
<td style=\"vertical-align: top; text-align: center;\">$Param<br>
</td>
<td style=\"vertical-align: top; text-align: center;\"><INPUT TYPE=\"checkbox\" NAME=\"Sh_$Param\" VALUE=\"1\" $checked><br>
</td>
</tr>
";
        }
        $Param_Table = $Param_Table . "
</tbody>
</table>
<hr>
";
    }

    # Insert the table of parameters into the HTML code
    $Content =~ s/INSERT_PARAMETERS_TABLE_HERE/$Param_Table/g;
    return;
}
########################

########################
# PrintOut
#
# Make nice plot and preset values of fit parameters nicely
########################
sub PrintOut {

    $RUNSType  = $in->param("RUNSType");
    if ($RUNSType) {
	$RunName = "RunFiles";
    } else {
	$RunName = "RunNumbers";
    }
    $RunNameValue = $in->param($RunName);

    $Content = $Content . "
<BODY BGCOLOR=\"$BGCOLOR\" BACKGROUND=\"$BACKGROUND_URL\" TEXT=\"$TEXT_COLOR_NORMAL\" onload=\"\">
<center>
<form accept-charset=utf-8 enctype=\"multipart/form-data\" action=\"$MAINPAGE\" method=\"post\">
<table cellpadding=\"2\" cellspacing=\"2\" border=\"2\" >
<tbody>
<tr>
<td colspan=\"7\" align=\"middle\" bgcolor=\"#E0E0E0\">
<font color=\"#FF0000\"><i>
INSERT_TITLE_HERE
</i></font>
</td>
</tr>
<tr>
<td colspan=\"1\" valign=\"top\" width=\"20%\">
<table cellspacing=\"2\" border=\"2\"  bgcolor=\"#E0E0E0\" width=\"100%\">
<tbody>
 <tr>
  <td>
   <select name=\"Minimization\" style=\"width: 90px\">
    <option $MinSel{\"MIGRAD\"} value=\"MIGRAD\">MIGRAD</option>
    <option $MinSel{\"MAX_LIKELIHOOD\"} value=\"MAX_LIKELIHOOD\nMINIMIZE\">MAX_LIKELIHOOD</option>
    <option $MinSel{\"MINIMIZE\"} value=\"MINIMIZE\">MINIMIZE</option>
    <option $MinSel{\"SIMPLEX\"} value=\"SIMPLEX\">SIMPLEX</option>
   </select>
  </td>
 </tr>
 <tr>
  <td>
   <select name=\"ErrorCalc\" style=\"width: 90px\">
    <option $ErrSel{\"HESSE\"} value=\"HESSE\">HESSE</option>
    <option $ErrSel{\"MINOS\"} value=\"MINOS\">MINOS</option>
   </select>
  </td>
 </tr>
 <tr>
  <td>
   <input type=\"submit\" name=\"go\" value=\"FIT\" style=\"width: 90px\">
  </td>
 </tr>
 <tr>
  <td>Theory Block</td>
 </tr>
 <tr>
  <td>
   <textarea name=\"Func_T_Block\" cols=\"20\" rows=\"4\">$Func_T_Block</textarea>
  </td>
 </tr>
 <tr>
  <td>Functions Block</td>
 </tr>
 <tr>
  <td>
   <textarea name=\"FunctionsBlock\" cols=\"20\">$FunctionsBlock</textarea>
  </td>
 </tr>
</tbody>
</table>
<td valign=\"bottom\" colspan=\"5\">
<center>
<table cellspacing=\"2\" border=\"2\" width=\"100%\">
<tbody>
 <tr>
  <img src=\"$OUTPUT/$FILENAME.png\" width=\"500\" align=top>
 </tr>
 <tr>
   <td colspan=\"3\"><font color=\"#FF0000\"><i>INSERT_CHI_LINE_HERE</i></font></td>
 </tr>
 <tr>
  <td colspan=\"3\">
   RUN numbers separated with commas, hyphens (from-to) or colons (from:to:step)<br>
   <input type=\"text\" name=\"$RunName\" value=\"$RunNameValue\"  size=\"70\">
  </td>
 </tr>
 <tr>
  <td>
   t<sub>i</sub>= <input type=\"text\" name=\"Tis\" value=\"$Tis\"  size=\"4\">
  </td>
  <td>
   t<sub>f</sub>= <input type=\"text\" name=\"Tfs\" value=\"$Tfs\" size=\"4\">
  </td>
  <td>
   Bin= <input type=\"text\" name=\"BINS\" value=\"$BINS\" size=\"4\">
  </td>
 </tr>
 <tr>
  <td colspan=\"3\">
   Fit type <select name=\"FitAsyType\">
     <option $FitAsyType{\"Asymmetry\"} value=\"Asymmetry\">Asymmetry</option>
     <option $FitAsyType{\"SingleHist\"} value=\"SingleHist\">Single Hist</option>
     <option $FitAsyType{\"AsymmetryGLB\"} value=\"AsymmetryGLB\">Asymmetry GLB</option>
   </select> 
   Histograms: <input type=\"text\" name=\"LRBF\" id=\"LRBF\" value=\"$LRBF\" size=\"8\">
  </td>
 </tr>
</tbody>
</table>
</center>
</td>
<td valign=\"top\" width=\"20%\">
<center>
<table cellspacing=\"2\" border=\"2\"  bgcolor=\"#E0E0E0\" width=\"100%\">
<tbody>
<tr>
<td>
FFT <input type=\"checkbox\" name=\"fftv\" value=\"y\" $fftvcheck>
<br>x-axis range<br>
Fi= <input type=\"text\" name=\"FRQMIN\" value=\"$Fi\" size=\"3\"> <br>
Ff= <input type=\"text\" name=\"FRQMAX\" value=\"$Ff\" size=\"3\"> <br>
Units=<select name=\"FUNITS\" style=\"width: 90px\">
 <option $FFTU{\"MHz\"} value=\"MHz\">MHz</option>
 <option $FFTU{\"Gauss\"} value=\"Gauss\">Gauss</option>
</select><br>
Apodization=<select name=\"FAPODIZATION\" style=\"width: 90px\">
 <option $FAPODIZATION{\"STRONG\"} value=\"STRONG\">STRONG</option>
 <option $FAPODIZATION{\"MEDIUM\"} value=\"MEDIUM\">MEDIUM</option>
 <option $FAPODIZATION{\"WEAK\"} value=\"WEAK\">WEAK</option>
 <option $FAPODIZATION{\"NONE\"} value=\"NONE\">NONE</option>
</select><br>
</td>
</tr>
<tr>
<td>
   Log x <input type=\"checkbox\" name=\"logx\" value=\"y\" $logxcheck>
</td>
</tr>
<tr>
<td>
   Log y <input type=\"checkbox\" name=\"logy\" value=\"y\" $logycheck>
</td>
</tr>
<tr>
<td>
   &tau; cor. <input type=\"checkbox\" name=\"ltc\" value=\"y\" $ltccheck>
</td>
</tr>
<tr>
<td>
X,Y range<br>
Xi= <input type=\"text\" name=\"Xi\" value=\"$Xi\" size=\"3\"> <br>
Xf= <input type=\"text\" name=\"Xf\" value=\"$Xf\" size=\"3\"> <br>
Yi= <input type=\"text\" name=\"Yi\" value=\"$Yi\" size=\"3\"> <br>
Yf= <input type=\"text\" name=\"Yf\" value=\"$Yf\" size=\"3\"> <br>
</td>
</tr>
<tr>
<td>
View Bin<br>
<input type=\"text\" name=\"ViewBin\" value=\"$ViewBin\" size=\"4\"> <br>
</td>
</tr>
 <tr>
  <td>
   <input type=\"submit\" name=\"go\" value=\"PLOT\" style=\"width: 90px\">
  </td>
 </tr>
 <tr>
  <td>
   <input type=\"submit\" name=\"go\" value=\"Restart\" style=\"width: 90px\">
  </td>
 </tr>
</tbody>
</table>
</center>
</td>
</tr>
<tbody></table>
<div style='height:30vh;overflow:auto; resize:vertical;'>
<table cellpadding=\"2\" cellspacing=\"2\" border=\"2\" >
<tbody>
<tr>
<td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 100px\"><a href=\"/MusrPlot/?file=$OUTPUT/$FILENAME\_par.dat\" target=\"\_blank\" title='Plot parameters using musrfit plotter'>Plot pars.</a></td><td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 90px\">Name</td><td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 90px\">Value</td><td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 90px\">Error</td><td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 90px\">Min</td><td style=\"background-color:#FFFF99;position: sticky; top: 2px;width: 90px\">Max</td>
</tr>
INSERT_PARAMETERS_TABLE_HERE
</tbody></table>
</td>
</tr>
</tbody>
</table>
</div>
PASS_ON_HTML_CODE
</form>
</center>
<hr>Download <a href=\"$OUTPUT/$FILENAME.msr\">$FILENAME.msr</a>.
<br>Download ascii file <a href=\"$OUTPUT/$FILENAME.dat.gz\">$FILENAME.dat.gz</a>.
<br>Download or <a href=\"/MusrPlot/?file=$OUTPUT/$FILENAME\_par.dat\" target=\"\_blank\">plot</a> parameters table file <a href=\"$OUTPUT/$FILENAME\_par.dat\">$FILENAME\_par.dat</a>.
<br>Download template msr file <a href=\"$OUTPUT/$RUNS[0]\_tmpl.msr\">$RUNS[0]\_tmpl.msr</a>.
<br>Debug information <a href=\"$OUTPUT/$FILENAME.out\">$FILENAME.out</a> and <a href=\"$OUTPUT/debug.txt\">debug.txt</a>.
";

    # Initialize the extra pass on parameters

    # Check log x and/or log y by default depending on previous setting
    if ( $fftv eq "y" ) {
        $fftvcheck = "checked";
    }
    else {
        $fftvcheck = $EMPTY;
    }
    if ( $logx eq "y" ) {
        $logxcheck = "checked";
    }
    else {
        $logxcheck = $EMPTY;
    }
    if ( $logy eq "y" ) {
        $logycheck = "checked";
    }
    else {
        $logycheck = $EMPTY;
    }

    # Check lifetime correction by default depending on previous settings
    if ( $ltc eq "y" ) {
        $ltccheck = "checked";
    }
    else {
        $ltccheck = $EMPTY;
    }

    # Reset log x and log y flags
    $fftv = "n";
    $logx = "n";
    $logy = "n";
    $ltc  = "n";

    # Open msr file
    $Param_Table = $EMPTY;
    open( IFILE,q{<}, "$REALDIR/$FILENAME.msr" );
    @lines = <IFILE>;
    close(IFILE);

    # Open mlog file in case of non-converging fit
    open( IMLOG,q{<}, "$REALDIR/$FILENAME.msr" );
    @lines_mlog = <IMLOG>;
    close(IMLOG);

    # Get Chi line and reformat
    @Chi_Line = grep { /chisq/ } @lines;
    if ( $TITLE eq $EMPTY || $TITLE eq "TITLE" ) { $TITLE = $lines[0]; }
    if ( $Chi_Line[0] eq $EMPTY ) { $Chi_Line[0] = "Fit did not converge yet"; }

# Remove comment lines
    @lines = grep {!/^\#/} @lines;
    @lines_mlog = grep {!/^\#/} @lines_mlog;
# Remove empty lines
    @lines = grep {/\S/} @lines;
    @lines_mlog = grep {/\S/} @lines_mlog;

# Identify different blocks
    $i=0;
    foreach $line (@lines)
    {
	if (grep {/FITPARAMETER/} $line) {$NFITPARAMETERS=$i;}
	if (grep {/THEORY/} $line) { $NTHEORY=$i;} 
	if ((grep {/RUN/} $line) & $NRUN==0) { $NRUN=$i;} 
	$i++;
    }
    @FPBlock=@lines[$NFITPARAMETERS+1..$NTHEORY-1];
    @TBlock=@lines[$NTHEORY+1..$NRUN-1];

    # Check if FPBlock contains "nan"s
    $FullFPBlock = join("\n",@FPBlock);
    if ( index($FullFPBlock, "nan") != -1) {
	# There is nan, take parameter values from the .mlog file
	 @FPBlock=@lines_mlog[$NFITPARAMETERS+1..$NTHEORY-1];
    }

    foreach my $line (@FPBlock) {
# TODO: Better treatement for FPBlock is to remove lines starting with # or empty.
	$line =~ s/\n//g;
	
	($tmp, $order, $Param, $value, $error, $errorp, $minvalue, $maxvalue) = split(/\s+/,$line);
# Take the number/order of the spectrum
	@itmp = split( /\_/, $Param );
		
	# If it is a multiple fit take also the run number
	if ( $itmp[1] ne $EMPTY ) {
	    if ( $RUNS[ $itmp[1] - 1 ] ne $RUN ) {
		$RUN  = $RUNS[ $itmp[1] - 1 ];
		$PRUN = $RUN;
	    }
	    else {
		$PRUN = $SPACE;
	    }
	}
		
	# Keep the values from the fit for the next round..
	$errParam = $erradd . $Param;
	$minParam = $Param . $minadd;
	$maxParam = $Param . $maxadd;
	if ( $value eq $EMPTY ) {
	    # Take original defaults
	    ( $Param_ORG, $tmp ) = split( '_', $Param );
	    $value    = MSR::Defaults{$Param_ORG};
	    $error    = MSR::Defaults{ join( $EMPTY, $erradd, $Param_ORG ) };
	    $minvalue = MSR::Defaults{ join( $EMPTY, $Param_ORG, $minadd ) };
	    $maxvalue = MSR::Defaults{ join( $EMPTY, $Param_ORG, $maxadd ) };
	} else {
	    # This is the trick to make an expression from the names :)
	    $$Param    = $value;
	    $$errParam = $error;
	    $$minParam = $minvalue;
	    $$maxParam = $maxvalue;
	}
	#	    $TC=$TC."$order,$Param,$value,$error,$errorp,$minvalue,$maxvalue<br>";
	$Param_Table = $Param_Table . "
<tr>
<td>$order</td>
<td bgcolor=\"#99CCFF\">$Param</td>
<td><input type=\"text\" name=\"$Param\" value=\"$value\" size=\"6\"></td>
<td><input type=\"text\" name=\"$erradd$Param\" value=\"$error\" size=\"6\"></td>
<td><input type=\"text\" name=\"$Param$minadd\" value=\"$minvalue\" size=\"6\"></td>
<td><input type=\"text\" name=\"$Param$maxadd\" value=\"$maxvalue\" size=\"6\"></td>
</tr>
";
    }
    $Content =~ s/INSERT_PARAMETERS_TABLE_HERE/$Param_Table/g;
    $Content =~ s/INSERT_CHI_LINE_HERE/$Chi_Line[0]/g;
    $Content =~ s/INSERT_TITLE_HERE/$TITLE/g;

    return;
}    #PrintOut
########################

########################
#This subroutine adds the parameters to pass on
########################
sub PassOn {
    $PassOn = $EMPTY;
    # Which parameters to pass on (keep) or reset
    foreach $One (@All) {
        if ( $One ne $EMPTY && $One ne "go" ) {
	    if ( $$One ne $EMPTY ) {
                $OneValue = $$One;
            } else {
                $OneValue = $in->param($One);
            }
	    $OneValue  =~ s/\n//g;

	    $skip=0;
	    # Skip Sh_ variables during SHARE step
	    if ($Step eq "SHARE" && substr($One,0,3) eq "Sh\_") { $skip=1 }
	    if ($skip != 1) {
		$PassOn = $PassOn
		    . "<input type=\"hidden\" name=\"$One\" id=\"$One\" value=\"$OneValue\">\n";
	    }
        }
    }
    $Content =~ s/PASS_ON_HTML_CODE/$PassOn/g;
    return;
}

########################
#This subroutine takes the parameters to pass in
########################
sub PassIn {
    $Step = $in->param("go");
    $RUNSType  = $in->param("RUNSType");
    $RunNumbers = $in->param("RunNumbers");
    $RunFiles = $in->param("RunFiles");

    # If the files flag is not set, treat RunNumbers as a list of numbers
    # Otherwise it is a list of file names
    if ($RUNSType) {
	@RUNS = split( /,/, $RunFiles );
    } else {
	# First remove illegal characters
	$RunNumbers =~ s/[\ \.\~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]/,/g;
	# then split commas
	@RUNS = split( /,/, MSR::ExpandRunNumbers($RunNumbers) );
    }

    $YEAR = $in->param("YEAR");
    %YEARSel = ( "$YEAR", "selected=\"selected\"" );
    
    $BeamLine = $in->param("BeamLine");
    if ( $BeamLine eq $EMPTY ) {
        %BeamSel = ( "LEM", "selected=\"selected\"" );
    } else {
        %BeamSel = ( "$BeamLine", "selected=\"selected\"" );
    }

    # Take the values which were passed on...
    $TITLE       = $in->param("TITLE");
    # If TITLE is empty or eq TITLE get it from the first run if you can
    if ( $TITLE eq $EMPTY || $TITLE eq "TITLE" || $T_Keep) {
	# This is a flag to keep setting TITLE to default
	$T_Keep = 1;
    } else {
	$T_Keep = 0;
    }

    $FILENAME = $in->param("FILENAME");
    #$FILENAME =~ s/[\.\~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]//g;
    #$FILENAME =~ s/ /_/g;
    # If the filename is empty or eq RUN put default
    if ( $FILENAME eq $EMPTY || $FN_Keep) {
	# Flag to keep default filename
	$FN_Keep = 1;
	if ( $RUNS[0] ne $EMPTY && !$RUNSType) {
	    ( $TMP1, $TMP2, $BLine ) = split( /\//, $BeamLine );
	    $FILENAME = 1*$RUNS[0] . "_" . $BeamLine . "_" . $YEAR;
	    $FILENAME =~ s/[\ \~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]//g;
	} else {
	    $FILENAME="TMP";
	}
    } else {
	$FN_Keep = 0;
    }
    $All{"FILENAME"} = $FILENAME;

    $Minimization = $in->param("Minimization");
    if ( $Minimization eq $EMPTY ) {
	%MinSel = ("MIGRAD", "selected=\"selected\"" );
    } else {
	%MinSel = ( "$Minimization", "selected=\"selected\"" );
    }

    $ErrorCalc = $in->param("ErrorCalc");
    if ( $ErrorCalc eq $EMPTY ) {
	%ErrSel = ("HESSE", "selected=\"selected\"" );
    } else {
	%ErrSel = ( "$ErrorCalc", "selected=\"selected\"" );
    }

    $FFTUnits = $in->param("FUNITS");
    if ( $FFTUnits eq $EMPTY ) {
	%FFTU = ("MHz", "selected=\"selected\"" );
    } else {
	%FFTU = ( "$FFTUnits", "selected=\"selected\"" );
    }

    $FFTApod = $in->param("FAPODIZATION");
    if ( $FFTApod eq $EMPTY ) {
	%FAPODIZATION = ("STRONG", "selected=\"selected\"" );
    } else {
	%FAPODIZATION = ( "$FFTApod", "selected=\"selected\"" );
    }

    $numComps = $in->param("numComps");
    @FitTypes = ();
    for ($i=1;$i<=$numComps;$i++) {
	$FitType = $in->param("FitType$i");
        if ( $FitType ne "None" ) { 
	    push( @FitTypes, $FitType ); 
	}
    }

    $BINS = $in->param("BINS");
    $Tis  = $in->param("Tis");
    $Tfs  = $in->param("Tfs");

    $Xi = $in->param("Xi");
    $Xf = $in->param("Xf");
    $Yi = $in->param("Yi");
    $Yf = $in->param("Yf");

    if ($Yi == $Yf) {$Yi="";$Yf="";}
    if ($Xi == $Xf) {$Xi="";$Xf="";}

    $Fi = $in->param("FRQMIN");
    $Ff = $in->param("FRQMAX");

    $ViewBin = $in->param("ViewBin");

    # Initail value if empty
    if ( $BINS eq $EMPTY ) { $BINS = 100; }
    if ( $Tis  eq $EMPTY ) { $Tis  = 0; }
    if ( $Tfs  eq $EMPTY ) { $Tfs  = 8; }

    $BINS =~ s/[\ \~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]/,/g;
    $Tis  =~ s/[\ \~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]/,/g;
    $Tfs  =~ s/[\ \~\/\&\*\[\;\>\<\^\$\(\)\`\|\]\'\@]/,/g;

    @BINVals = split( /,/, $BINS );
    @TiVals  = split( /,/, $Tis );
    @TfVals  = split( /,/, $Tfs );

    $j = 0;
    foreach my $Ti (@TiVals) {
        #Check conditions on numbers
        #    $BINS = int($BINS);
        $BIN = $BINVals[$j];
        $Tf  = $TfVals[$j];
        if ( $BIN <= 0 ) { $BINVals[$j] = 100; }
        if ( $Ti > $Tf ) {
            $tmp        = $Tf;
            $TfVals[$j] = $Ti;
            $TiVals[$j] = $tmp;
        }
        if ( $Ti < 0  || $Ti > 13 ) { $TiVals[$j] = 0.0; }
        if ( $Tf <= 0 || $Tf > 13 ) { $TfVals[$j] = 10; }

        $LRBF = $in->param("LRBF");
        if ( $LRBF eq $EMPTY ) {
            $LRBF = "1 5,3 7";
        }
        @Hists = split( /,/, $LRBF );

        $RRF = $in->param("RRF");
        ++$j;
    }
    $BINS = join( $COMMA, @BINVals );
    $Tis  = join( $COMMA, @TiVals );
    $Tfs  = join( $COMMA, @TfVals );

    $fftv = $in->param("fftv");
    $logx = $in->param("logx");
    $logy = $in->param("logy");
    $ltc  = $in->param("ltc");

    # Initail value if empty
    if ( $fftv ne "y" ) {
        $fftv = "n";
    } else {
        $fftvcheck = "checked";
    }
    if ( $logx ne "y" ) {
        $logx = "n";
    } else {
        $logxcheck = "checked";
    }
    if ( $logy ne "y" ) {
        $logy = "n";
    } else {
        $logycheck = "checked";
    }
    if ( $ltc ne "y" ) {
        $ltc = "n";
    } else {
        $ltccheck = "checked";
    }

    # Fit type: Asymmetry, Single Histogram, or Asymmetry GLB
    $FitAsyType = $in->param("FitAsyType");
    if ( $FitAsyType eq $EMPTY ) {
        %FitAsyType = ( "AsymmetryGLB", "selected=\"selected\"" );
    } else {
        %FitAsyType = ( "$FitAsyType", "selected=\"selected\"" );
    }

    $Func_T_Block = $in->param("Func_T_Block");
    if ($Func_T_Block eq $EMPTY || $Func_T_Block==0 ) {undef $Func_T_Block;}  
    $FunctionsBlock = $in->param("FunctionsBlock");
    if ($FunctionsBlock eq $EMPTY || $FunctionsBlock==0) {undef $FunctionsBlock;}
    if ($Step eq "PLOT") {
        $datestring = localtime();
        $country = `/usr/bin/geoiplookup $ENV{REMOTE_ADDR}`;
        system("echo 'Time=$datestring\nIP=$ENV{REMOTE_ADDR} from $country Year=$YEAR, BeamLine=$BeamLine, RUNs=$RunNumbers \n' >>  $subdir/$MYLOG");
    }
    return;
}
