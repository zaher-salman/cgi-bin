# Package to be used with TrimSP javascript GUI
# This is different from the package used for the QT GUI.
package TrimSPjs;

use strict;

# Function: Create and return input file for the Trim.SP simulation
# binary
sub CreateInpFile()
{
    use Chem;

# Chemical formulas will be parsed on the fly for each layer. However,
# I will check if all the layers have inputs for composition,
# thickness and density. If not complain and stop :)
    
# Values of Z,A as well as other needed parameters are obtained from
# Chem.pm.
    
# Feed in all parameters from the GUI
    my %All = %{$_[0]};

# This is the form of the begining of the input file:
    my $TemplateFile=
	    " ProjZ ProjAM valEnergy sigEnergy valAngle sigAngle parEF parESB parSHEATH parERC
 numberProj ranSeed ranSeed ranSeed z0 parRD dz parCA parKK0 parKK0R parKDEE1 parKDEE2 parIPOT parIPOTR parIRL";
 
# Then comes the number of layers (new format) for example 4 layers:
#  N_Layers=4
    $TemplateFile=$TemplateFile."\n"."N_Layers=numLayer"."\n";
    			 
# Then loop over the layers and for each give the following structure
   my $TemplateLayer=
 "L1d L1rho L1CK
 L1ELZ1 L1ELZ2 L1ELZ3 L1ELZ4 L1ELZ5
 L1ELW1 L1ELW2 L1ELW3 L1ELW4 L1ELW5
 L1ELC1 L1ELC2 L1ELC3 L1ELC4 L1ELC5
 L1ELE1 L1ELE2 L1ELE3 L1ELE4 L1ELE5
 L10301 L10302 L10303 L10304 L10305
   0.0000   0.0000   0.0000   0.0000   0.0000
 L1ELST11 L1ELST21 L1ELST31 L1ELST41 L1ELST51
 L1ELST12 L1ELST22 L1ELST32 L1ELST42 L1ELST52
 L1ELST13 L1ELST23 L1ELST33 L1ELST43 L1ELST53
 L1ELST14 L1ELST24 L1ELST34 L1ELST44 L1ELST54
 L1ELST15 L1ELST25 L1ELST35 L1ELST45 L1ELST55
 ";

# Projectile properties
    $All{"ProjZ"}=sprintf("%6.2f",Chem::Zof($projectile));    
    $All{"ProjAM"}=sprintf("%6.2f",Chem::Massof($projectile));

# This is the flag for checking layers    
    my $Check=0;
    my $Layer="";
    my $Li="";
# Loop over layers an create appropriate values
    for (my $i=1;$i<=$All{"NL"};$i++){
	$Li = "L".$i;
	$Layer = $TemplateLayer;
	$Layer =~ s/L1/$Li/g;
	$TemplateFile=$TemplateFile.$Layer;
	$Check=0;
# Composition of layers	
	my $LComp="L".$i."Comp";
	my $Comp = this->{ui}->layerTable->item($i-1,0)->text();
	$All{$LComp} = $Comp;
	my %LElComp=Chem::parse_formula($Comp);
	foreach my $key (keys %LElComp) {
# Check if composition is understood
	    if ($key eq "" || Chem::Zof($key) eq "") {
		$Check++;
	    }  
	}
	if ($Comp eq "") {$Check++;}
# Write composition to results file header

# Densities of layers
	my $Lrho="layer".$i."rho";
	my $rho = 1*$All{$Lrho};
	$All{$Lrho}=sprintf("%6.2f",$rho);
	if ($rho eq "") {$Check++;}
	
# Thickness of layers
	my $Ld ="L".$i."d";
	my $d = $All{$Ld};
	$All{$Ld}=sprintf("%8.2f",$d);
	if ($d eq "") {$Check++;}
	
# Sanity check, is the layer supposed to have value? are they all there?
	$ErrMsg = 1;
	if ($Check!=0 & $i<=$All{"NL"}) {
	    my $ErrMsg="Error: Layer $i is empty. Expecting it to be defined!\n";
	    print STDERR $ErrMsg;
	    return $ErrMsg;
	}
	
	my $tmp = "L".$i."CK";
	$All{$tmp}=sprintf("%6.2f",1.0);
	
	my $Sum = 0;
	foreach (keys %LElComp) { 
	    $Sum=$Sum+$LElComp{$_};
	}
	if ($Sum==0) {$Sum=1;}
	
	my @Els = keys %LElComp;
	
	for (my $NEl=1;$NEl<=5;$NEl++) {
	    my $El = $Els[$NEl-1];
	    my $LEkey = "L".$i."EL";
	    my $ElZ = Chem::Zof($El);
	    my $ElW = Chem::Massof($El);
	    my $ElC = $LElComp{$El}/$Sum;
	    my $ElE = Chem::Elastof($El);
	    my $El030 = 30;
	    if ($El eq "") { $El030 = 0.0;}

	    $All{$LEkey."Z".$NEl}=sprintf("%8.4f",$ElZ);
	    $All{$LEkey."W".$NEl}=sprintf("%8.4f",$ElW);
	    $All{$LEkey."C".$NEl}=sprintf("%8.4f",$ElC);
	    $All{$LEkey."E".$NEl}=sprintf("%8.4f",$ElE);
	    $All{"L".$i."030".$NEl}=sprintf("%8.4f",$El030);
	    
	    my $ElST = Chem::Stopicru($El);
	    my @ElSTs = split (/,/,$ElST);
	    my $j=1;
	    foreach (@ElSTs) {
		$LEkey = "L".$i."ELST".$NEl.$j;
		$j++;
		$All{$LEkey}=sprintf("%11.6f",$_);
	    }
	}
    }
    
    foreach my $key (keys %All) {
	if ($All{$key} ne ""){
	    $TemplateFile =~ s/$key/$All{$key}/;
# Seed repeats three times
	    if ($key eq "Seed") { $TemplateFile =~ s/$key/$All{$key}/g;}
	}
    }
    return $TemplateFile;
}


# Subroutine: Start the simulation with the current parameters
sub StartSequenceOne()
{
    my %All = CollectValues();
    my @SValues=();
    my @SdzValues=();
    my $cmd="";
    
    if (!$ENV{'TRIMBIN'}) {
# If trim.sp binary is not defined give warning and return
	my $Warning = Qt::MessageBox::information( this, "Warning!",
			"Warning:\n TrimSP binary is not found.\n Define using the Configuration tab.");
	return(0);

    }


# Cleanup from old files
    if (-e "ausgabe1.inp") {
	system("rm -f ausgabe*");
    }
    
    my $Progress=0;
    if ($All{"scanSeq"}) {
	if ($All{"radioList"}) {
	    @SValues=split(/,/,$All{"scanList"});
	    @SdzValues=split(/,/,$All{"scanListdz"});
	    if ($#SValues == $#SdzValues) {$All{"SdzFlag"}=1;}
	} elsif ($All{"radioLoop"}) {
	    for (my $Val=$All{"scanFrom"};$Val<=$All{"scanTo"};$Val=$Val+$All{"scanStep"}) {
		@SValues=(@SValues,$Val);
	    }
	}
	
	my $ScanName = "";
	my $ScanVar = "";
	if ($All{"comboScan"}==0) {
	    $ScanName = "E";
	    $ScanVar = "valEnergy";
	} elsif ($All{"comboScan"}==1) {
	    $ScanName = "SigE";
	    $ScanVar = "sigEnergy";
	} elsif ($All{"comboScan"}==2) {
	    $ScanName = "Angle";
	    $ScanVar = "valAngle";
	} elsif ($All{"comboScan"}==3) {
	    $ScanName = "SigAngle";
	    $ScanVar = "sigAngle";
	} elsif ($All{"comboScan"}==4) {
	    $ScanName = "N";
	    $ScanVar = "numberProj";
	} elsif ($All{"comboScan"}==5) { 
	    $ScanName = "Ld".$All{"scandL"};
	    $ScanVar = "layer".$All{"scandL"}."d";
	}
	
	my $ScanAttrib = child("Qt::LineEdit",$ScanVar);
	my $iScan=0;
	foreach (@SValues) {
	    if ($All{"comboScan"}==5) {
		layerTable->setText($All{"ScandL"}-1,2,$_);	
	    } else {
		$ScanAttrib->setText($_);
	    }
	    if ( $All{"SdzFlag"} == 1) {
		if ($All{"comboScan"}==0) {
		    this->{ui}->dz->setText($SdzValues[$iScan]);
		} elsif ($All{"comboScan"}==3) {
		    this->valEnergy->setText($SdzValues[$iScan]);
		}
	    }
	    my $eingabe1=CreateInpFile();
	    if ($eingabe1 eq "ERROR") {return 0;}
	    my $FILENAME=$All{"fileNamePrefix"}."_".$ScanName.$_;
	    open (INPF,q{>}, "$FILENAME.inp" );
	    print INPF $eingabe1;
	    close(INPF);
# Use windoz version	    
#	    system("cp $FILENAME.inp eingabe1.inp; wine TrimSP7L.exe");
# Use Linux version	    
	    $Progress=$Progress+90/$#SValues;
	    this->{ui}->progress->setValue($Progress);
	    this->{ui}->progress->update();
	    $cmd = "cp $FILENAME.inp eingabe1.inp; ".$ENV{'TRIMBIN'};
	    system($cmd);
	    
	    foreach ("err","out","rge") {
		system("mv -f ausgabe1.$_ $FILENAME.$_");
	    }
# Not needed if work path is changed	    
#	    $cmd="mv -f $FILENAME.* ".$All{"workPath"};
#	    system($cmd);
	    $iScan++;
	}
    } else {
# For a single run
	my $eingabe1=CreateInpFile();
	if ($eingabe1 eq "ERROR") {return 0;}
	my $FILENAME=$All{"fileNamePrefix"};
	open (INPF,q{>}, "$FILENAME.inp" );
	print INPF $eingabe1;
	close(INPF);
	$Progress=20;
	this->{ui}->progress->setValue($Progress);
	
# Use windoz version	    
#              system("cp $FILENAME.inp eingabe1.inp; wine TrimSP7L.exe");
# Use Linux version
	$cmd = "cp $FILENAME.inp eingabe1.inp; ".$ENV{'TRIMBIN'};
	system($cmd);
	foreach ("err","out","rge") {
	    system("mv -f ausgabe1.$_ $FILENAME.$_");
	}
	$Progress=90;
	this->{ui}->progress->setValue($Progress);
# If we change work directory this is not needed	
#	$cmd="mv -f $FILENAME.* ".$All{"workPath"};
#	system($cmd);
    }
# Move the fort.33 file into the subdirectory and change its name
    $cmd="rm -f eingabe1.inp; mv -f fort.33 ".$All{"workPath"}."/".$All{"fileNamePrefix"}."_Seq_Results.dat";
    system($cmd);
    $Progress=100;
    this->{ui}->progress->setValue($Progress);
   return 1;
}	

# Subroutine: Open a configuration file
sub OpenFile()
{
# Types of different input
    my %Types = ();
    $Types{"numLayer"}="Qt::SpinBox";
    $Types{"projComboBox"}="Qt::ComboBox";
    $Types{"scanSeq"}="Qt::RadioButton";
    $Types{"comboScan"}="Qt::ComboBox";
    $Types{"scandL"}="Qt::SpinBox";
    $Types{"radioList"}="Qt::RadioButton";
    $Types{"scanListdz"}="Qt::LineEdit";
    $Types{"radioLoop"}="Qt::RadioButton";
    $Types{"scanSeq"}="Qt::GroupBox";
    
    my $file=Qt::FileDialog::getOpenFileName(
	this,
	"Choose an initialization file",
	".",
	"Initialization file (.* *.*)");
    
# If the user gave a valid filename try to read it
    if ($file ne "") {
	open (INF,q{<},"$file" );
	my @lines = <INF>;
	close(INF);
	my $Attrib="";
	foreach my $line (@lines) {
# Remove white spaces
	    $line =~ s/\s+//g;
	    my @InitPar = split (/=/,$line);
# Check it is not empty or title line
	    if ($InitPar[0] || $InitPar[1]) {
		if (!$Types{$InitPar[0]}) {
		    $Types{$InitPar[0]}="Qt::LineEdit";
		}
# Get widget by name
		$Attrib = child($Types{$InitPar[0]},$InitPar[0]);
# if the widget exists
		if ($Attrib){
		    if ($Types{$InitPar[0]} eq "Qt::SpinBox") {
			$Attrib->setValue($InitPar[1]);
		    } elsif ($Types{$InitPar[0]} eq "Qt::RadioButton" || $Types{$InitPar[0]} eq "Qt::GroupBox") {
			if($InitPar[1]) { 
			    $Attrib->setChecked(1);
			} else {
			    $Attrib->setChecked(0);
			}
		    } elsif ($Types{$InitPar[0]} eq "Qt::ComboBox") {
			$Attrib-> setCurrentIndex($InitPar[1]);
		    } else {
			$Attrib->setText($InitPar[1]);
		    }
		}
	    }
	}
    }
}

# Subroutine: Save configuration file to TrimSP.cfg
sub SaveFile()
{
    my %All = CollectValues();
    my $InitFile="
[Layers]
numLayer=$All{'numLayer'}
layer1Comp=$All{'layer1Comp'}
layer1rho=$All{'layer1rho'}
layer1d=$All{'layer1d'}
layer2Comp=$All{'layer2Comp'}
layer2rho=$All{'layer2rho'}
layer2d=$All{'layer2d'}
layer3Comp=$All{'layer3Comp'}
layer3rho=$All{'layer3rho'}
layer3d=$All{'layer3d'}
layer4Comp=$All{'layer4Comp'}
layer4rho=$All{'layer4rho'}
layer4d=$All{'layer4d'}
layer5Comp=$All{'layer5Comp'}
layer5rho=$All{'layer5rho'}
layer5d=$All{'layer5d'}
layer6Comp=$All{'layer6Comp'}
layer6rho=$All{'layer6rho'}
layer6d=$All{'layer6d'}
layer7Comp=$All{'layer7Comp'}
layer7rho=$All{'layer7rho'}
layer7d=$All{'layer7d'}

[ProjectileParameters]
projComboBox=$All{'projComboBox'}
numberProj=$All{'numberProj'}
z0=$All{'z0'}
dz=$All{'dz'}
valEnergy=$All{'valEnergy'}
sigEnergy=$All{'sigEnergy'}
valAngle=$All{'valAngle'}
sigAngle=$All{'sigAngle'}
ranSeed=$All{'ranSeed'}

[Files]
fileNamePrefix=$All{'fileNamePrefix'}
workPath=$All{'workPath'}

[ScanSequence]
scanSeq=$All{'scanSeq'}
comboScan=$All{'comboScan'}
radioList=$All{'radioList'}
scanList=$All{'scanList'}
scanListdz=$All{'scanListdz'}
radioLoop=$All{'radioLoop'}
scanFrom=$All{'scanFrom'}
scanTo=$All{'scanTo'}
scanStep=$All{'scanStep'}
";

# Save to default file name "TriumSP.cfg"
     my $file = "TrimSP.cfg";
     open (OUTF,q{>},"$file" );
     print OUTF (" $InitFile");
     close(OUTF);
}

# Subroutine: Plot implantation profiles using root macro
sub PlotProfiles()
{
    my $Path=this->{ui}->workPath->text();
    my $files_ref=Qt::FileDialog::getOpenFileNames(
	this,
	"Select RGE files to plot",
	$Path,
	"RGE Files (*.rge)");

    my @files_tmp = @$files_ref;
# Do "natural" sorting on the file name list
# This will produce (hopefully) a plot with largest needed depth scale 
    my @files = grep {s/(^|\D)0+(\d)/$1$2/g,1} sort grep {s/(\d+)/sprintf"%06.6d",$1/ge,1} @files_tmp;

    my $filenames=join(",",@files);
    
    if ($filenames ne "" ) {
	my $TrimPath = $ENV{'PERLLIB'};	
# Now that we have the file list send it to root macro for plotting.
	my $cmd='root -n -l "'.$TrimPath.'/plotRge.C(\"'.$filenames.'\")"&';
	my $pid=system($cmd);	
    }
}

# Subroutine: Plot implantation fractions in the different layers using
# a root macro
sub PlotFraction()
{
    my $Path=this->{ui}->workPath->text();
    my $file=Qt::FileDialog::getOpenFileName(
	this,
	"Choose a sequence data file",
	$Path,
	"Implantation sequence file (*.dat)");
        
    if ($file ne "" ) {
	my $TrimPath = $ENV{'PERLLIB'};	
# Now that we have the file name send it to root macro for plotting.
	my $cmd='root -n -l "'.$TrimPath.'/plotFrc.C(\"'.$file.'\")"&';

	my $pid=system($cmd);	
    }

}

1;
