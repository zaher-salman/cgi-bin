#!/usr/bin/perl 
#
# Copyright Zaher Salman 2023-.
# zaher.salman@psi.ch

######## Relevant subdirectories and urls ########
# May need modification on different servers
$trimbin="timeout 300 /usr/lib/trimsp/resources/app/trimspNL";

# Nothing need to be changed from here on
#######################################################################
######## Main body of code ########
use CGI;

my $in = new CGI;
# Collect parameters and their values from CGI
my ($name,$value);
$content = "";
my $filename = "";
my $lcount = 0;
foreach $name ($in->param) {
    $value=$in->param($name);
    if ($name eq "fn") {
	# This is the file name
	$filename = $value;
	@prefix = split(/\//,$filename);
	$randName = $prefix[2];
	$FILENAME = $prefix[3];
    } elsif ( index($name, "line") == 0) {
	# This is a line start counting
	#if ($lcount <=14) {
	    # Limit number of lines (15) to avoid abuse
	    $content = $content.$value."\n";
	#}
	$lcount++;
    }
}
$workPath = "/var/www/html/tmp/".$randName;
if (-d $workPath) {
# Directory exists, do nothing
} else {
    $cmd="mkdir ".$workPath;
    system($cmd);
}
$filename = "/var/www/html".$filename;
$fname = $filename;
$fname =~ s/.inp//;
open (OUTF,q{>}, $filename);
print OUTF ($content);
close(OUTF);

print ("Content-type: text/ascii \n\n");
if  (index($filename,"_Seq_Results.dat") != -1) {
    # This is the final call of a scan
    $cmd = "chdir $workPath; rm -f fort.33 edist;"; 
    $cmd = $cmd."tar -cvzf ../$randName.tgz *; cd ..; rm -rf $randName;";
} else {
    # This is a call to simulate one step in a scan
    $cmd = "chdir $workPath; cp $filename eingabe1.inp; $trimbin >> /dev/null 2>&1; rm -f eingabe1.inp;";
    $cmd = $cmd."mv -f ausgabe1.rge $fname.rge;";
    $cmd = $cmd."mv -f ausgabe1.out $fname.out;";
    $cmd = $cmd."mv -f ausgabe1.err $fname.err;";
}
system($cmd);
#print "Written file $filename\n";
#print $content;
print "command $cmd\n";
foreach $name ($in->param) {
    $value=$in->param($name);
    print $name,"=",$value,"\n";
}
    
exit(0);

