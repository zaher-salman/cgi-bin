#!/usr/bin/perl 
#
# Copyright 2009- (C) by Zaher Salman. 
# zaher.salman@psi.ch


#######################################################################
######## Relevant subdirectories and urls (global)             ########
# May need modification on different servers
$MAINPAGE = "/cgi-bin/SearchDB.cgi";
# The path to musrfit binaries. Needed for file conversion (any2many)
$MUSRFITDIR = "/usr/local/bin";
# Maximum number of files to convert/download
$MAXFILES = 200;

# Keep log file of all calls
$MYLOG = "/var/www/html/tmp/mylog.keep";

#$SUMM_DIR="/mnt/data/nemu/summ/";
$SUMM_DIR="/afs/psi.ch/project/nemu/data/summ/";
#%DATADIRS = ("LEM",   "/mnt/data/nemu/his/",
%DATADIRS = ("LEM",   "/afs/psi.ch/project/nemu/data/his/",
	     "GPS",   "/afs/psi.ch/project/bulkmusr/data/gps/",
	     "Dolly", "/afs/psi.ch/project/bulkmusr/data/dolly/",
	     "GPD", "/afs/psi.ch/project/bulkmusr/data/gpd/",
	     "ALC", "/afs/psi.ch/project/bulkmusr/data/alc/",
	     "LTF",   "/afs/psi.ch/project/bulkmusr/data/ltf/",
	     "FLAME",   "/afs/psi.ch/project/bulkmusr/data/flame/",
	     "HAL", "/afs/psi.ch/project/bulkmusr/data/hifi/",
	     "ALL", "/afs/psi.ch/project/bulkmusr/data/*/");
# Database is available here
#%DBDIR=("LEM","/mnt/data/nemu/log/",
%DBDIR=("LEM","/afs/psi.ch/project/nemu/data/log/",
	"GPS","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"Dolly","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"GPD","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"ALC","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"LTF","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"FLAME","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"HAL","/afs/psi.ch/project/bulkmusr/olddata/search/",
	"ALL","/afs/psi.ch/project/bulkmusr/olddata/search/");
# Information available since
%MinYears=("LEM","2001",
	   "GPS","1993",
	   "Dolly","1998",
	   "GPD","1993",
	   "ALC","1993",
	   "HAL","2011",
	   "FLAME","2020",
	   "LTF","1995",
	   "ALL","1993");
@BeamLines=("LEM","GPS","LTF","Dolly","GPD","ALC","HAL","FLAME","ALL");

# And to deal with old names of bulk muons
%AltArea=("GPS","PIM3",
	  "LTF","PIM3",
	  "FLAME","PIM3",
	  "ALC","PIE3",
	  "Dolly","PIE1",
	  "GPD","MUE1",
	  "HAL","hifi");
#######################################################################

#######################################################################
######## Main body of code ########
use CGI;
#use CGI::Log;

my $in = new CGI;

# First check if there is a request for summary file
$link = $in->param("RunSumm");
$AREA = $in->param("AREA");
if ($link ne "") { 
    &SpitSumm;
}

# Contains temporary debug output
$TC="";

# Defaul minimum year
# $Min_Year=$MinYears{$AREA};
$Min_Year=1993;
# Get current year
($sec,$min,$hour,$mday,$mon,$Cur_Year,$wday,$yday,$isdst) = localtime(time());
$Cur_Year = $Cur_Year+1900;

# Take input parameters and values for search
# Check if there is a request for search
$Do = $in->param("go");
# Take year value. Make sure the script is not being abused, only allowed years
$YEAR = $in->param("YEAR");
if ($YEAR > $Cur_Year || $YEAR < $Min_Year) {$YEAR=$CurYear;}
$AFTER = $in->param("AFTER");
$BEFORE = $in->param("BEFORE");
# Get RUN value/range
$RUN = $in->param("RUN");
$Rmin = $in->param("Rmin");
$Rmax = $in->param("Rmax");
# Get search phrase
$Phrases = $in->param("Phrases");

# Take input parameters and values for conversion/download
# Total number of selected runs
$Counter = $in->param("Counter");
# Conversion output file format
$OUTFormat = $in->param("FORMAT");
# Histogram groups in output files
$HisGroup = $in->param("HGROUP");
# Binning factor. Must be a number
$REBIN = 1*$in->param("REBIN");

# Hidden option to select all
$SELECTALL = $in->param("SELECTALL");

# This is for logging, not essential but good to track things
if ($Do ne "") {
    $datestring = localtime();
    $datestring =~s/[^\x00-\x7f]//g;
    $country = `/usr/bin/geoiplookup $ENV{REMOTE_ADDR}`;
    $country =~s/[^\x00-\x7f]//g;
    system("echo 'Time=$datestring \n IP=$ENV{REMOTE_ADDR} from $country Year=$YEAR, Area=$AREA, After=$AFTER, Before=$BEFORE, Run=$RUN, Rmin=$Rmin, Rmax=$Rmax, Prase=($Phrases), Counter=$Counter, OutFormat=$OUTFormat, Action=$Do\n' >>  $subdir/$MYLOG");
}
# Here we can creat a logfile...
#Log->debug("This is a test $AREA");

# Establish default format of data files 
$INFormat="MusrRoot";

$Years="<option $YEARSel{\"0\"} value=\"\"></option>";
# Creat a drop down menu for years from 1994 until current
for ($i=$Cur_Year; $i >= $Min_Year; $i--) 
{
    $Years = $Years."     <option $YEARSel{\"$i\"} value=\"$i\">$i</option>";
}

#$BEAMLINES="<option value=\"\"> $BeamLine </option>";
$BEAMLINES="";
foreach $BeamLine (@BeamLines)
{
    $BEAMLINES=$BEAMLINES."<option value=\"$BeamLine\"> $BeamLine </option>";
}

# Stages of the application
if ($Do eq "") {
    # First start with initial search page
    &InitialPage;
    &SpitHTML;
} elsif ($Do eq "Search") {
    # Do not allow empty searches
    if ($Phrases eq "" && $Rmin eq "" && $Rmax eq "" && $RUN eq "" ) {
	&SpitHTML;
    } else {
	# Search and display results
	if ($AREA eq "LEM") {
	    &ExtractInfoLEM;
	} elsif ($AREA eq "ALL") {
	    # Call both LEM and Bulk search
	    &ExtractInfoBulk;
	    &ExtractInfoLEM;
	} else {
	    &ExtractInfoBulk;
	}
	&SpitHTML;
    }
} elsif ($Do eq "SingleFileAscii") {
    $AllFiles=$DATADIRS{$AREA}."d".$YEAR."/tdc/deltat_tdc_".lc($AREA)."_".sprintf("%04d",$RUN).".bin";
    $INFormat = "PSI-BIN";
    if ($YEAR >= 2023) {
        $AllFiles=$DATADIRS{$AREA}."d".$YEAR."/tdc/root/deltat_tdc_".lc($AREA)."_".$YEAR."_".sprintf("%04d",$RUN).".root";
        $INFormat = "MusrRoot";
    }
    &SpitArchiveConv($INFormat,"ASCII",$REBIN);
} elsif ($Do eq "TAR" || $Do eq "Download") {
    # Provide archive of selected data files
    if ($Counter>0) {
	# loop over all results and collect selected files
	@Files=();
	@RunNumbers=();
	for ($i=1;$i<=$Counter;$i++) {
	    ($Checked,$Run,$AREA,$YEAR)=split(/,/,$in->param("Chk$i"));
	    if ($Checked ne "") {
		$Content=$Content.$Checked."<br>\n";
		@Files=(@Files,$Checked);
		@RunNumber=(@RunNumbers,$Run);
	    }
	}
    }

    if ($#Files <= $MAXFILES-1) {
	# Make a list of files, space separated
	$AllFiles=join(" ",@Files);
	# Export selected files in native format
	# Put limit on number of files (~200)
	&SpitArchiveConv($INFormat,$OUTFormat,$REBIN);
    } else {
	@Messages=("Too many files are selected.","A maximum of $MAXFILES is allowed!");
	&SpitMessage(@Messages);
    }
} elsif ($Do eq "LogBook") {
    # Provide archive of selected data files
    if ($Counter>0) {
	# loop over all results and collect selected files
	@Files=();
	@RunNumbers=();
	for ($i=1;$i<=$Counter;$i++) {
	    ($Checked,$Run,$AREA,$YEAR)=split(/,/,$in->param("Chk$i"));
	    if ($Checked ne "") {
		$Content=$Content.$Checked."<br>\n";
		@Files=(@Files,$Checked);
		@RunNumber=(@RunNumbers,$Run);
	    }
	}
    }

    # Make a list of files, space separated
    $AllFiles=join(",",@Files);
    #&SpitArchiveConv($INFormat,$OUTFormat,$REBIN);
    &SpitTextLogbook;
}  elsif ($Do eq "PLOT") {
    # Send list of files to musrfit.cgi?RUNSType=1&RunFiles=...
    if ($Counter>0) {
	# loop over all results and collect selected files
	@Files=();
	@RunNumbers=();
	# Initialize flags
	# YEARSFLAG signals runs from the same year (=year) or not (=0)
	# AREAFLAG signals runs from the same beam line (=AREA) or not (=0)
	$AREAFLAG="";
	$YEARSFLAG="";
	for ($i=1;$i<=$Counter;$i++) {
	    ($Checked,$Run,$AREA,$YEAR)=split(/,/,$in->param("Chk$i"));
	    if ($Checked ne "") {
		# For the first checked run initialize year and area
		if ($AREAFLAG eq "" & $YEARSFLAG eq "") {
		    $AREAFLAG=$AREA;
		    $YEARSFLAG=$YEAR;
		} else {
		    if ($AREAFLAG ne $AREA) {
			$AREAFLAG = 0;
		    }
		    if ($YEARSFLAG != $YEAR) {
			$YEARSFLAG =0;
		    }
		}
		$Content=$Content.$Checked."<br>\n";
		@Files=(@Files,$Checked);
		@RunNumbers=(@RunNumbers,$Run);
	    }
	}
    }

    if ($#Files <= $MAXFILES-1) {
	if ($YEARSFLAG > 0 & $AREAFLAG ne "0") {
	    &musrfit_runs(@RunNumbers);
	} else {
	    &musrfit(@Files);
	}
    }
}
exit(0);

#######################################################################
# This function displays some html content in $Conetent
sub SpitHTML {
print ("Content-type: text/html; charset=iso-8859-1  \n\n");
print ("<html>
<head>
<title>$TITLE</title>
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; no-cache; charset=iso-8859-8-I\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
</head>
<BODY BGCOLOR=\"$BGCOLOR\" BACKGROUND=\"$BACKGROUND_URL\" TEXT=\"$TEXT_COLOR_NORMAL\">
$TC
$Content
<HR>
If you find any bugs please send me an <a href=\"mailto:zaher.salman\@psi.ch\">email</a>.<br>
</body>
</html>
");
}
#######################################################################

#######################################################################
# This function converts file formats as requested, archives them 
# and sends to user.
# At the moment it requires write access to disk on server, hopefully
# with development of any2many will be able to do the conversion and
# archiving on the fly and direct output to user directly.
sub SpitArchiveConv {
    ($INFormat,$OUTFormat,$REBIN)=@_;

# Create a randomized string for a subdirectory name
    @chars=('a'..'z','A'..'Z','0'..'9','_');
    $subdir="/tmp/";
    foreach (1..5) {
       $subdir.=$chars[rand @chars];
    }	

    if ($INFormat eq $OUTFormat & $REBIN==0) {
# No conversion/rebinning required. Copy files to random string path
	$cmd="cp $AllFiles $subdir/";
    } else {
# Use any2many to create converted files in the random string path
	if ($REBIN == 0) {
	    # No binning
	    $cmd="$MUSRFITDIR/any2many -f $AllFiles -c $INFormat $OUTFormat $HisGroup -p $subdir/";
	} else {
	    # With binning
	    $cmd="$MUSRFITDIR/any2many -f $AllFiles -c $INFormat $OUTFormat -rebin $REBIN $HisGroup -p $subdir/";
	}
    }

# This is common part for converted and non-converted files
    $cmd="mkdir $subdir;".$cmd." > $subdir/out.txt";
    $cmdres=system($cmd);
# Leave some debuging info in the subdirectory too
    system("echo 'IN=$INFormat OUT=$OUTFormat AREA=$AREA \n command=$cmd' >>  $subdir/out.txt");
    
# Compress and archive all files pipe to user
    print ("Content-Type:application/octet-stream; name=\"Data.tgz\" \n");
#    print ("Content-Type:application/x-download\n");
    print ("Content-Disposition:attachment; filename=\"Data.tgz\" \n\n");
#    open(TAR,"cd $subdir;tar -czf - * --exclude out.txt|");
    open(TAR,"cd $subdir;tar -czf - * |");
    while (<TAR>) {
	print $_;
    }
    close(TAR);
# Clean up local disk
    system("rm -rf $subdir");
}
#######################################################################

#######################################################################
# This function creates the html content for the initial page
sub InitialPage {

# Prepare body of the HTML code
    $Content = $Content."
<form enctype=\"multipart/form-data\" action=\"$MAINPAGE\" method=\"post\">
<H1> Search PSI <EM>&micro;SR </EM> Runs </H1>
<HR>

<table>
  <tr>
   <td><i>Area</i>:</td>
   <td><select  style='width:100%' ALIGN=MIDDLE name=\"AREA\">
       INSERT_BEAMLINES_HERE
       </select>
   </td>
   <td></td>
   <td></td>
  </tr>
  <tr>
   <td><i>Year</i>:</td>
   <td><select style='width:100%' name=\"YEAR\">
       INSERT_YEARS_HERE
       </select>
   </td>
   <td>or from:  
       <input TYPE=\"text\" name=\"AFTER\" value=\"\" SIZE=\"6vw\">
   </td>
   <td>to:
       <input TYPE=\"text\" name=\"BEFORE\" value=\"\" SIZE=\"6vw\"> 
   </td>
  </tr>
  <tr>
   <td><i>Run #</i>:</td>
   <td><input TYPE=\"text\" name=\"RUN\" SIZE=\"6vw\"></td>
   <td>or from:
       <input TYPE=\"text\" name=\"Rmin\" SIZE=\"6vw\">
   </td>
   <td>to:
       <input TYPE=\"text\" name=\"Rmax\" SIZE=\"6vw\"></td>
  </tr>
  <tr>
   <td><i>Run Title</i>:</td>
   <td colspan=\"3\"><input TYPE=\"text\" name=\"Phrases\" SIZE=\"41vw\"></td>
  </tr>
</table>
<HR>
<input type=\"hidden\" name=\"go\" value=\"Search\">
<input type=\"reset\" value=\"Reset\"><input type=\"submit\" value=\"Search\"></form>
<b>Instructions</b>:<br>
<ul>
<li>You may leave any of the search fields blank, it will then match all entries.</li>
<li>You may look for multiple phrases by making them comma separated.</li>
<li>Spaces in the phrases are also considered.</li>
</ul>
<b>Please note that PSI-MDU and PSI-BIN formats are not supported. Address any questions regarding these to your local contact.</b>
";

# Insert the years into the HTML code
    $Content =~ s/INSERT_YEARS_HERE/$Years/g;
# Insert the BeamLines into the HTML code
    $Content =~ s/INSERT_BEAMLINES_HERE/$BEAMLINES/g;
}
#######################################################################



#######################################################################
# This function is used to extract information from LEM log files
sub ExtractInfoLEM {
    @PHRASES=split(/,/,$Phrases);

# Year limit
    $YearLimit="none";
    if ($AFTER eq "" & $BEFORE eq "" & $YEAR ne "") {
	$YearLimit="one";
	$AFTER=$YEAR;
	$BEFORE=$YEAR;
    } elsif ( $AFTER ne "" & $BEFORE eq "" & $YEAR eq "") {
	$YearLimit="bottom";
	$BEFORE=$Cur_Year;
    } elsif ( $AFTER eq "" & $BEFORE ne "" & $YEAR eq "") {
	$YearLimit="top";
	$AFTER=$Min_Year;
    } elsif ( $AFTER eq "" & $BEFORE eq "" & $YEAR eq "") {
	$YearLimit="none";
	$AFTER=$Min_Year;
	$BEFORE=$Cur_Year;
    } elsif ( $AFTER ne "" & $BEFORE ne "" & $YEAR eq "") {
	$YearLimit="both";
    }
    
# Fix obviuos mistakes
    if ($YearLimit eq "both" & $AFTER>$BEFORE) {
	$tmp=$AFTER;
	$AFTER=$BEFORE;
	$BEFORE=$tmp;
    }

# Run limit
    $RunLimit="none";
    if ($Rmin eq "" & $Rmax eq "" & $RUN ne "") {
	$RunLimit="one";
	$Rmin=$RUN;
	$Rmax=$RUN;
    } elsif ( $Rmin ne "" & $Rmax eq "" & $RUN eq "") {
	$RunLimit="bottom";
	$Rmax=99999;
    } elsif ( $Rmin eq "" & $Rmax ne "" & $RUN eq "") {
	$RunLimit="top";
	$Rmin=0;
    } elsif ( $Rmin eq "" & $Rmax eq "" & $RUN eq "") {
	$RunLimit="none";
	$Rmin=0;
	$Rmax=99999;

    } elsif ( $Rmin ne "" & $Rmax ne "" & $RUN eq "") {
	$RunLimit="both";
    }
    
# Fix obviuos mistakes
    if ($RunLimit eq "both" & $Rmin>$Rmax) {
	$tmp=$Rmin;
	$Rmin=$Rmax;
	$Rmax=$tmp;
    }

#    $TC=$TC."Rmin=$Rmin<br>Rmax=$Rmax<br>";

# Create YEARS array
    @YEARS=();
    for ($i=$AFTER;$i<=$BEFORE;$i++) {
	$YEARS[$i-$AFTER]=$i;
    }

# Until here its common to LEM and Bulk

# Check for multiple terms to search
    foreach $YEAR (@YEARS) {
	$DBFILE=$DBDIR{"LEM"}.$YEAR."/runlog.txt";
	# $DBFILE=$DBDIR{$AREA}.$YEAR."/runlog.txt";
	
	open(DBF,"$DBFILE");
	@Lines=<DBF>;
	close(DBF);
	foreach $Phrase (@PHRASES){
	    @Lines = grep { /$Phrase/ } @Lines;
	}
	push(@TotLines,@Lines);
    } 
    
# Refine search for runs
    @ResLines=();
    foreach $Line (@TotLines) {
	@Words=split(/\t/,$Line);
	if ($Words[0]>=$Rmin & $Words[0]<=$Rmax) {
	    push(@ResLines,"$Line");
	}
    }
    @Lines=@ResLines;


    $Content=$Content."
<script language=\"JavaScript\">
function toggle(source) {
  checkboxes = document.getElementsByClassName('filechk');
  if (checkboxes.length > 50) {
    alert('Take it easy, you can select a maximum of 50 files.');
    source.checked = false;
    return;
  }
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
function checkConv(e) {
  if (e.value == 'PSI-BIN') {
    alert('Please make sure you are binning the data!');
  }
}
</script>
<form enctype=\"multipart/form-data\" action=\"$MAINPAGE\" method=\"post\" target=\"new\">
<table>
<tr>
<b>Please note that PSI-MDU and PSI-BIN formats are not supported. Address any questions regarding these to your local contact.</b>
</tr>
<tr>
<td><button style='width:100%;' type=\"submit\" name=\"go\" value=\"TAR\">Download</button></td>
<td> selected as 
<select style='float:right;width:50%' ALIGN=MIDDLE onchange=\"checkCov(this);\" name=\"FORMAT\">
<option value=\"MusrRoot\"> MusrRoot </option>
<option value=\"PSI-BIN\"> PSI-BIN </option>
<option value=\"ASCII\"> ASCII </option>
<option value=\"MUD\"> MUD </option>
<option value=\"NeXus1-HDF4\"> NeXuS1-HDF4 </option>
<option value=\"NeXus2-HDF4\"> NeXuS2-HDF4 </option>
<option value=\"NeXus1-HDF5\"> NeXuS1-HDF5 </option>
<option value=\"NeXus2-HDF5\"> NeXuS2-HDF5 </option>
</select>
</td>
<td> Bin by: <input TYPE=\"text\" name=\"REBIN\" value=\"\" SIZE=\"6\"> histogram groups 
<select ALIGN=MIDDLE name=\"HGROUP\">
<option value=\"-h 0, 20\"> NPP&PPC Red</option>
<option value=\"-h 40, 60\"> NPP&PPC Green</option>
<option value=\"-h 0, 20, 40, 60\"> NPP&PPC (Red/Green)</option>
<option value=\"-h 0\"> NPP Red</option>
<option value=\"-h 40\"> NPP Green</option>
<option value=\"-h 0, 40\"> NPP Red/Green</option>
<option value=\"-h 20\"> PPC Red</option>
<option value=\"-h 60\"> PPC Green</option>
<option value=\"-h 20, 60\"> PPC Red/Green</option>
</select>
</td>
</tr>
<tr>
<td><button style='width:100%;' type=\"submit\" name=\"go\" value=\"PLOT\">Plot Seleted</button></td>
<td><button style='float:right;width:50%;' type=\"submit\" name=\"go\" value=\"LogBook\">LogBook</button></td>
<td><input style='float:right;width:50%;' Type=\"button\" value=\"Reset\" onClick=\"history.go(-1);return true;\"></td>
</tr>
</table>
<table border=\"1\">
<tr><td><input type=\"checkbox\" name=\"SelectAll\" onchange=\"toggle(this);\"></td><td></td><td><b>Year</b></td><td><b>RUN</b></td><td><b>STATS</b></td><td><b>Title</b></td></tr>
";

    $DATA_DIR=$DATADIRS{"LEM"};

# This is the lines counter
    $Counter=0;
# Check for request to select all
    if ($SELECTALL == 1) { 
	$Checked = "checked";
    } else {
	$Checked = "";
    }
    foreach $Line (@ResLines) {
	$Counter=$Counter+1;
	$Line=~ s/\n//g;
	@Words=split(/\t/,$Line);
# Extract the year
	($Date,$tmp)=split(/::/,$Words[1]);
	$YEAR=1*substr($Date,10);

	$RUN=$Words[0];
	$RUN=~ s/\s+//g;
	if ($RUN < 10000) {
	    $RUN = sprintf("%04d",$RUN);
	} 
	$link=$SUMM_DIR.$YEAR."/lem".substr($YEAR,2)."\_".$RUN;
	$datafile=$DATA_DIR.$YEAR."/lem".substr($YEAR,2)."\_his\_".$RUN.".root";
	if ($Words[3] ne "") {
	    $Content=$Content."<tr><td><input type=\"checkbox\" name=\"Chk$Counter\" class=\"filechk\" value=\"$datafile,$RUN,LEM,$YEAR\" $Checked></td><td>$Counter</td><td>$YEAR</td><td><button type=\"submit\" name=\"RunSumm\" value=\"$link\">$RUN</button></td><td>$Words[2]</td><td>$Words[3]</td></tr>\n";
	} else {
	    @tmp=split(/\s+/,$Words[1]);
	    $Content=$Content."<tr><td><input type=\"checkbox\" name=\"Chk$Counter\" class=\"filechk\" value=\"$datafile,$RUN,LEM,$YEAR\" $Checked></td><td>$Counter</td><td>$YEAR</td><td><button type=\"submit\" name=\"RunSumm\" value=\"$link\">$RUN</button></td><td>$tmp[$#tmp]</td><td>$Words[2]</td></tr>\n";
	}
    }


    $Content=$Content."
</table>
<input type=\"hidden\" name=\"Counter\" value=\"$Counter\">
<input type=\"hidden\" name=\"AREA\" value=\"$AREA\">
</form>";
}
#######################################################################


#######################################################################
# This function is used to extract information from Bulk log files
sub ExtractInfoBulk {
    @PHRASES=split(/,/,$Phrases);

# Year limit
    $YearLimit="none";
    if ($AFTER eq "" & $BEFORE eq "" & $YEAR ne "") {
	$YearLimit="one";
	$AFTER=$YEAR;
	$BEFORE=$YEAR;
    } elsif ( $AFTER ne "" & $BEFORE eq "" & $YEAR eq "") {
	$YearLimit="bottom";
	$BEFORE=$Cur_Year;
    } elsif ( $AFTER eq "" & $BEFORE ne "" & $YEAR eq "") {
	$YearLimit="top";
	$AFTER=$Min_Year;
    } elsif ( $AFTER eq "" & $BEFORE eq "" & $YEAR eq "") {
	$YearLimit="none";
	$AFTER=$Min_Year;
	$BEFORE=$Cur_Year;

    } elsif ( $AFTER ne "" & $BEFORE ne "" & $YEAR eq "") {
	$YearLimit="both";
    }
    
# Fix obviuos mistakes
    if ($YearLimit eq "both" & $AFTER>$BEFORE) {
	$tmp=$AFTER;
	$AFTER=$BEFORE;
	$BEFORE=$tmp;
    }

# Run limit
    $RunLimit="none";
    if ($Rmin eq "" & $Rmax eq "" & $RUN ne "") {
	$RunLimit="one";
	$Rmin=$RUN;
	$Rmax=$RUN;
    } elsif ( $Rmin ne "" & $Rmax eq "" & $RUN eq "") {
	$RunLimit="bottom";
	$Rmax=9999;
    } elsif ( $Rmin eq "" & $Rmax ne "" & $RUN eq "") {
	$RunLimit="top";
	$Rmin=0;
    } elsif ( $Rmin eq "" & $Rmax eq "" & $RUN eq "") {
	$RunLimit="none";
	$Rmin=0;
	$Rmax=9999;

    } elsif ( $Rmin ne "" & $Rmax ne "" & $RUN eq "") {
	$RunLimit="both";
    }
    
# Fix obviuos mistakes
    if ($RunLimit eq "both" & $Rmin>$Rmax) {
	$tmp=$Rmin;
	$Rmin=$Rmax;
	$Rmax=$tmp;
    }

# Create YEARS array
    @YEARS=();
    for ($i=$AFTER;$i<=$BEFORE;$i++) {
	$YEARS[$i-$AFTER]=$i;
    }

# Until here its comon to LEM and Bulk

# Check for multiple terms to search
    foreach $YEAR (@YEARS) {
	$area=lc $AREA;
	$DBFILE=$DBDIR{$AREA}.$YEAR."/*.tabs";
	@Lines=`cat $DBFILE`;
	
	foreach $Phrase (@PHRASES){
	    @Lines = grep { /$Phrase/ } @Lines;
	}
	if ($AREA ne "ALL") {
# Refine search for beamlines
	    @Lines1 = grep { /_$area/ } @Lines;
	    @Lines2 = grep { /_$AltArea{$AREA}/ } @Lines;
	    @Lines=(@Lines1,@Lines2);
	} 
	push(@TotLines,@Lines);
    }

    

# Refine search for runs
    @ResLines=();
    foreach $Line (@TotLines) {
	@Words=split(/\s+/,$Line);
	if ($Words[1]>=$Rmin & $Words[1]<=$Rmax) {
	    push(@ResLines,"$Line");
	}
    }
    @Lines=@ResLines;

    $DefaultFormat = "";
    if ($AREA eq "HAL") {$DefaultFormat = "selected";}

    $Content=$Content."
<script language=\"JavaScript\">
function toggle(source) {
  checkboxes = document.getElementsByClassName('filechk');
  if (checkboxes.length > 50) {
    alert('Take it easy, you can select a maximum of 50 files.');
    source.checked = false;
    return;
  }
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
function header_pop(filename) {
     alert(filename);
}
function checkConv(e) {
  if (e.value == 'PSI-BIN') {
    alert('Please make sure you are binning the data!');
  }
}
</script>
<form enctype=\"multipart/form-data\" action=\"$MAINPAGE\" method=\"post\" target=\"new\">
<table>
<tr>
<b>Please note that PSI-MDU and PSI-BIN formats are not supported. Address any questions regarding these to your local contact.</b>
</tr>
<tr>
<td><button style='width:100%;' type=\"submit\" name=\"go\" value=\"TAR\">Download</button></td>
<td> selected as 
<select style='float:right;width:50%;' ALIGN=MIDDLE onchange=\"checkConv(this);\" name=\"FORMAT\">
<option value=\"MusrRoot\"> MusrRoot </option>
<option value=\"PSI-BIN\"> PSI-BIN </option>
<option value=\"ASCII\"> ASCII </option>
<option value=\"MUD\"> MUD </option>
<option value=\"NeXus1-HDF4\"> NeXuS1-HDF4 </option>
<option value=\"NeXus2-HDF4\"> NeXuS2-HDF4 </option>
<option value=\"NeXus1-HDF5\"> NeXuS1-HDF5 </option>
<option value=\"NeXus2-HDF5\"> NeXuS2-HDF5 </option>
<option value=\"WKM\"> WKM </option>
<option $DefaultFormat value=\"PSI-MDU\"> PSI-MDU </option>
</select>
</td>
<td>Bin by: <input TYPE=\"text\" name=\"REBIN\" value=\"\" SIZE=\"6\"></td>
</tr>
<tr>
<td><button style='width:100%;' type=\"submit\" name=\"go\" value=\"PLOT\">Plot Seleted</button></td>
<td><button style='float:right;width:50%;' type=\"submit\" name=\"go\" value=\"LogBook\">LogBook</button></td>
<td><input style='float:right;width:50%;' Type=\"button\" value=\"Reset\" onClick=\"history.go(-1);return true;\"></td>
</tr>
</table>
<table border=\"1\">
<tr><td><input type=\"checkbox\" name=\"SelectAll\" onchange=\"toggle(this);\"></td><td></td><td><b>Year</b></td><td><b>RUN</b></td><td><b>Sample</b></td><td><b>Temperature</b></td><td><b>Field</b></td><td><b>Title</b></td><td>File</td></tr>
";

#    $DATADIRS{$AREA}

# This is the lines counter
    $Counter=0;
# Check for request to select all
    if ($SELECTALL == 1) { 
	$Checked = "checked";
    } else {
	$Checked = "";
    }
    foreach $Line (@ResLines) {
	$Counter=$Counter+1;
	$Line=~ s/\n//g;
#	$Line=~ s/ {2,}/\t/g;
#	@Words=split(/\s+/,$Line);
	@Words=split(/\t/,$Line);
# Extract the year
	$Date=$Words[1];
	$YEAR=1*substr($Date,7);
	if ($YEAR>90) {
	    $YEAR=1900+$YEAR;
	} else {
	    $YEAR=2000+$YEAR;
	}
#	$YEAR=$Words[4];

	$RUN=$Words[0]*1;
	if ($RUN < 10000) {
	    $RUN = sprintf("%04d",$RUN);
	}

	$link="";
# Take tdc by default ..
	$datafile=$DATADIRS{$AREA}."d".$YEAR."/tdc/".$Words[3];
#	$datafile=$DATADIRS{"GPS"}."d".$YEAR."/tdc/".$Words[3];
	if($Words[3] =~ m/pta/i) {
# Check if it is a pta file, but if you have to do that then tdc did not exist!
	    if (($YEAR <= 2008 && $AREA eq "LTF") || ($YEAR <= 2007)) {
		# No pta subdir was for LTF <=2008 and for GPS,Dolly,GPD <=2007
		$datafile=$DATADIRS{$AREA}."d".$YEAR."/".$Words[3];
	    } else {
		$datafile=$DATADIRS{$AREA}."d".$YEAR."/pta/".$Words[3];
	    }		
	}

	# Take root files if available
	if ($YEAR >= 2023) {
            $Words[3] =~ s/.bin/.root/;
            $stupid = "_" . $YEAR . "_" . $RUN;
            $Words[3] =~ s/_$RUN/$stupid/;
            if ($AREA eq "GPS" || $AREA eq "GPD") {
                $Words[3] =~ s/deltat_/deltatbc_/;
            } 
            $datafile=$DATADIRS{$AREA}."d".$YEAR."/tdc/root/".$Words[3];
        }
        $Filename = $Words[3];

	
# The rest is of couse not tab separated. Why make tings easy when they can be hard :)	    
	$Sample = substr($Words[4],0,10);
	$Temperature = substr($Words[4],11,10);
	$Field = substr($Words[4],22,10);
	$Title=substr($Words[4],44);

	$Content=$Content."<tr><td><input type=\"checkbox\" name=\"Chk$Counter\" class=\"filechk\" value=\"$datafile,$RUN,$AREA,$YEAR\" $Checked></td><td>$Counter</td><td>$YEAR</td><td><button type=\"submit\" name=\"RunSumm\" value=\"$datafile\")\">$RUN</button></td><td>$Sample</td><td>$Temperature</td><td>$Field</td><td>$Title</td><td>$Filename</td></tr>\n";
    }

    $Content=$Content."
</table>
<input type=\"hidden\" name=\"Counter\" value=\"$Counter\">
<input type=\"hidden\" name=\"AREA\" value=\"$AREA\">
</form>";
}
#######################################################################


#######################################################################
# This function provide summary file of selected LEM run
sub SpitSumm {
    if ($AREA eq "LEM") {
	# Need safety, not any file is allowed to read!!!!!
	# Add ".summ" to the link to make sure that only these files are read
	$link=$link.".summ";

	# link to selected run is in $link
	open(SummF,"$link");
	@SummText=<SummF>;
	close(SummF);
        $header = join("",@SummText);
    } else {
	#$cmd="$MUSRFITDIR/dump_header -c -fn $link";
	#@SummText = system($cmd);
        $header=`$MUSRFITDIR/dump_header -c -fn $link`;
        $header =~ s/-@\d+//g;
    }
    print ("Content-type: text/plain; charset=iso-8859-1  \n\n");
    print $header;
    exit(0);
}
#######################################################################

#######################################################################
# This function gives a message box
sub SpitMessage {

    @Messages=@_;
    $Message1=join(" ",@Messages);
    $Message2=join("<br>",@Messages);
    
print ("Content-type: text/html; charset=iso-8859-1  \n\n");
print (
"<html>
<head>
<title>Too many files selected</title>
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; no-cache; charset=iso-8859-8-I\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
</head>
<body bgcolor=\"$BGCOLOR\" BACKGROUND=\"$BACKGROUND_URL\" TEXT=\"$TEXT_COLOR_NORMAL\" onLoad=alert(\'$Message1\')>
$Message2
</body>
</html>
");
}

#######################################################################
# This function send run filename to musrfit.cgi
sub musrfit {

    @Files = @_;
    $AllFiles=join(",",@Files);
    
print ("Content-type: text/html; charset=iso-8859-1  \n\n");
print ("<html>
<head>
<title>Plot data</title>
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; no-cache; charset=iso-8859-8-I\">
<META http-equiv=\"refresh\" content=\"0; url=http://musruser.psi.ch/cgi-bin/musrfit.cgi?RUNSType=1&FitAsyType=Asymmetry&RunFiles=$AllFiles&YEAR=$YEARSFLAG&BeamLine=$AREAFLAG\"> 
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
</head>
<body>
</body>
</html>
");
}
#######################################################################


#######################################################################
# This function send run numbers to musrfit
sub musrfit_runs {

    @Files = @_;
    $AllFiles=join(",",@Files);
    
print ("Content-type: text/html; charset=iso-8859-1  \n\n");
print ("<html>
<head>
<title>Plot data</title>
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; no-cache; charset=iso-8859-8-I\">
<META http-equiv=\"refresh\" content=\"0; url=http://musruser.psi.ch/cgi-bin/musrfit.cgi?YEAR=$YEARSFLAG&BeamLine=$AREAFLAG&RunNumbers=$AllFiles\"> 
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
</head>
<body>
</body>
</html>
");
}
#######################################################################

# Function to extract information from the header
sub parseHeader {
    my ($header, $orig_pattern, $orig_endwith) = @_;
    my $pattern = quotemeta $orig_pattern;
    my $endwith = quotemeta $orig_endwith;
    my @Lines = split(/\n/,$header);
    # Extract information using Perl
    my ($result) = grep { /$pattern/ } @Lines;
    $result =~ s/\t/ /g;
    
    #my ($key,$value) = split(/: /, $result, 2);
    #$result = $value;
    $result =~ s/.*?$pattern//;
    if ($orig_pattern ne "Run Start Time:") {
        $result =~ s/$endwith.*//;
    } else {
        $result =~ s/$endwith.*//;
    }
    #$result =~ s/-@.*$//g;
    $result =~ s/^\s+|\s+$//g;
    return $result;
}


#######################################################################
# This function spits out a text logbook
sub SpitTextLogbook {
    @Files = split (/,/,$AllFiles);
    my $logbook = sprintf("%-7s%-10s%-10s%-10s%-9s%-23s%-10s%-10s%-15s\n",
                          "Run","Tset(K)","Tsam(K)","B(G)","MEv.","Start Time & Date","Sample",
                          "Orient.","Comments");
    print ("Content-type: text/plain; charset=iso-8859-1  \n\n");
    foreach $file (@Files) {
        $file =~ s/^\s+|\s+$//g;
        my $header = `$MUSRFITDIR/dump_header -c -fn $file`;
        my @parts = split /\./, $file;
        my $ext = lc($parts[-1]);
        if ($ext eq "root") {
            $runNumber = parseHeader($header, "Run Number:", " -@1");
            $sampleTemp = sprintf("%.2f",parseHeader($header, "Sample Temperature:", "+-"));
            $sampleTempsp = sprintf("%.2f",parseHeader($header, "; SP:", "\n"));
            #$sampleTemp0 = sprintf("%.2f",parseHeader($header, "Sample Temperature [0]:", "+-"));
            #$sampleTemp1 = sprintf("%.2f", parseHeader($header, "Sample Temperature [1]:", "+-"));
            #$sampleTemp2 = sprintf("%.2f", parseHeader($header, "Sample Temperature [2]:", "+-"));
            $totalCountsGroup1 = sprintf("%.1f", parseHeader($header, "total counts of group 1:", ", ") / 1000000);
            $runStartTime = parseHeader($header, "Run Start Time:", " -@0");
            $sampleMagneticField = parseHeader($header, "Sample Magnetic Field:", "G");
            $sampleName = parseHeader($header, "Sample Name:", " -@0");
            $sampleOrientation = parseHeader($header, "Sample Orientation:", " -@0");
            $runTitle = parseHeader($header, "Run Title:", " -@0");
        } else {
            $runNumber = parseHeader($header, "Run Number         :", "\n");
            $sampleTemp = sprintf("%.2f",parseHeader($header, "Sample Temp. 1     :", " K"));
            #$sampleTempsp = sprintf("%.2f",parseHeader($header, "; SP:", "\n"));
            #$sampleTemp0 = sprintf("%.2f",parseHeader($header, "Sample Temperature [0]:", "+-"));
            #$sampleTemp1 = sprintf("%.2f", parseHeader($header, "Sample Temperature [1]:", "+-"));
            #$sampleTemp2 = sprintf("%.2f", parseHeader($header, "Sample Temperature [2]:", "+-"));
            $totalCountsGroup1 = sprintf("%.1f", parseHeader($header, "No of Events       :", "\n") / 1000000);
            $runStartTime = parseHeader($header, "Run Start Time     :", "\n");
            $sampleMagneticField = parseHeader($header, "Sample Mag. Field  :", "G");
            $sampleName = parseHeader($header, "Sample Name        :", "\n");
            $sampleOrientation = parseHeader($header, "Sample Orientation :", "\n");
            $runTitle = parseHeader($header, "Run Title          :", "\n");            
        }
        
        # Provide default values for empty or unavailable information
        $runNumber           //= "N/A";
        $runStartTime        //= "N/A";
        $sampleTemp0         //= "N/A";
        $sampleTemp1         //= "N/A";
        $sampleTemp2         //= "N/A";
        $totalCountsGroup1   //= "N/A";
        $sampleMagneticField //= "N/A";
        $sampleName          //= "N/A";
        $sampleOrientation   //= "N/A";
        $runTitle            //= "N/A";
        
        $logbook .= sprintf("%-7s%-10s%-10s%-10s%-9s%-23s%-10s%-10s%-15s\n",
                            $runNumber,
                            $sampleTempsp,
                            $sampleTemp,
                            $sampleMagneticField,
                            $totalCountsGroup1,
                            $runStartTime,
                            $sampleName,
                            $sampleOrientation,
                            $runTitle
            );
    }

    print $logbook;
}
