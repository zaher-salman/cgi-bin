# README #

These are server side CGI applications, meaning that they are useless without access to database and data files. They are currently installed on http://musruser.psi.ch/. If you wish to use/adapt to your own server please contact me. Otherwise try them here:

* http://musruser.psi.ch/cgi-bin/musrfit.cgi

* http://musruser.psi.ch/cgi-bin/SearchDB.cgi

* http://musruser.psi.ch/cgi-bin/TrimSP.cgi

### What is this repository for? ###

* Quick summary:
This repository includes all files needed for cgi-bin script used as a
service for LMU/PSI users of the SmuS facilities. The major components
are a database search throgh muSR data run files, a web based fitting
and viewing application, and calculation of muon implantation profiles
for low energy muons.

### Documentation ###
* http://musruser.psi.ch/Documentation/musrfit/musrfit-doc.html
* http://musruser.psi.ch/Documentation/SearchDB/SearchDB-doc.html

### Contribution guidelines ###

* Allow access to muSR database at other facilities
* Use and report any bugs or other issues

### Who do I talk to? ###

* Zaher Salman <[zaher.salman@psi.ch](mailto:zaher.salman@psi.ch)>
* LEM Group at PSI
