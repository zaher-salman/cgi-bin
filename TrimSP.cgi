#!/usr/bin/perl 
#
# Copyright 2017- (C) by Zaher Salman. 
# zaher.salman@psi.ch

#######################################################################
######## Relevant subdirectories and urls ########
# May need modification on different servers
$MAINPAGE = "/cgi-bin/TrimSP.cgi";
$trimbin="timeout 300 /usr/local/bin/trimspNL";

# Nothing need to be changed from here on
#######################################################################
######## Main body of code ########
use CGI;
#use TrimSPjs;
#use CGI::Log;

my $in = new CGI;


# Start from default values just in case
my %All=();
$All{"numLayer"}= 1;
$All{"L1Comp"}= "SrTiO3";
$All{"L1rho"}= 5.12;
$All{"L1d"}= 10000;
$All{"ProjType"}= "Muon";
$All{"numberProj"}= 1000;
$All{"z0"}= 0;
$All{"dz"}= 20;
$All{"valEnergy"}= 2000;
$All{"sigEnergy"}= 450;
$All{"valAngle"}= 0;
$All{"sigAngle"}= 15;
$All{"ranSeed"}= 78741;
$All{"scanSeq"}= "";
$All{"scanFrom"}= "1000";
$All{"scanStep"}= "2000";
$All{"scanTo"}= "14000";

#$All{""}= ;

# Create a randomized string for a file name
my @chars=('a'..'z','A'..'Z','0'..'9','_');
my $rchar="";
foreach (1..5) {
    $rchar.=$chars[rand @chars];
}
# Random file name and path	
$All{"fileNamePrefix"}=$rchar;
$All{"workPath"}="/tmp/".$rchar;
$All{"links"}="";

# Some fixed or normally kept constant parameters
$All{"parEF"}=0.5;
$All{"parESB"}=0.0;
$All{"parSHEATH"}=0.0;
$All{"parERC"}=0.0;
$All{"parRD"}=50.0;
$All{"parCA"}=1.0;
$All{"parKK0"}=2;
$All{"parKK0R"}=2;
$All{"parKDEE1"}=4;
$All{"parKDEE2"}=3;
$All{"parIPOT"}=2;
$All{"parIPOTR"}=1;
$All{"parIRL"}=0;
# format additional parameters
$All{"parEF"}=sprintf("%8.2f",$All{"parEF"});
$All{"parESB"}=sprintf("%8.2f",$All{"parESB"});
$All{"parSHEATH"}=sprintf("%8.2f",$All{"parSHEATH"});
$All{"parERC"}=sprintf("%8.2f",$All{"parERC"});
$All{"parRD"}=sprintf("%5.0f.",$All{"parRD"});
$All{"parCA"}=sprintf("%6.2f",$All{"parCA"});
$All{"parKK0"}=sprintf("%3d",$All{"parKK0"});
$All{"parKK0R"}=sprintf("%3d",$All{"parKK0R"});
$All{"parKDEE1"}=sprintf("%3d",$All{"parKDEE1"});
$All{"parKDEE2"}=sprintf("%3d",$All{"parKDEE2"});
$All{"parIPOT"}=sprintf("%3d",$All{"parIPOT"});
$All{"parIPOTR"}=sprintf("%3d",$All{"parIPOTR"});
$All{"parIRL"}=sprintf("%2d",$All{"parIRL"});

# Initilize layer information
my @Comps=();
my @Rhos=();
my @Ds=();

# Collect parameters and their values from CGI
my ($name,$value);
foreach $name ($in->param) {
    $value=$in->param($name);
    $All{$name}=$value;
    # Collect layer information from previous call
    if ($name =~ m/L\d+Comp/) {@Comps=(@Comps,'"'.$value.'"')} # Composition
    if ($name =~ m/L\d+rho/) {@Rhos=(@Rhos,'"'.$value.'"')}    # Density
    if ($name =~ m/L\d+d/) {@Ds=(@Ds,'"'.$value.'"')}          # thickness
    # Check if it is a scan or single profile
    if ($name eq "scanSeq" && $All{$name} eq "on") {$All{$name}="checked";}
#    $TC=$TC.$name."==".$in->param($name)."\\n";	
}

# Initialize TrimSP simulation results
# rgedata are profiles and seqdata are fractions
my ($rgedata_ref,$seqdata_ref)=("","");
if ($in->param("go") eq "start") {
    # Preferably scan over values manually
    ($rgedata_ref,$seqdata_ref) = StartSequence(\%All);
}

if ($rgedata_ref eq "") {
# Failed sequence!
    $All{"links"}="";
} else {
    $All{"links"}="Download results: <a href=\"/tmp/$All{'fileNamePrefix'}.tgz\">$All{'fileNamePrefix'}.tgz</a>.";
    my @rgedata = @$rgedata_ref;
    my @seqdata = @$seqdata_ref;
    # Is the scan checkbox on? if not change scan parameters accordingly
    if ($All{"scanSeq"} eq "checked") {
	for (my $Val=$All{"scanFrom"};$Val<=$All{"scanTo"};$Val=$Val+$All{"scanStep"}) {
	    @SValues=(@SValues,$Val/1000);
	}
    } else {
	@SValues=($All{"valEnergy"}/1000);
    }
    $All{"Rows"}=PlotProfiles(\@rgedata,\@SValues);
    $All{"Fracs"}=PlotProfiles(\@seqdata);
}
#$TC = $All{"Rows"}.$All{"Fracs"};
$TC =~ s/\n/<br>/g;

$All{"Comps"} = join(",",@Comps);
$All{"Rhos"} = join(",",@Rhos);
$All{"Ds"} = join(",",@Ds);
$TITLE ="TrimSP Results";

&SpitGUI(\%All);
exit(0);

#######################################################################
# Subroutine: Start the simulation with the current parameters
sub StartSequence()
{
    my %All = %{$_[0]};
    my @SValues=();
    my @SdzValues=();
    my $cmd="";
    
    $trimbin="timeout 300 /usr/local/bin/trimspNL";
# Create a subdirectory where all input/output files are saved   
    if (-d $All{"workPath"}) {
# Directory exists, do nothing
    } else {
	$cmd="mkdir ".$All{"workPath"};
	system($cmd);
    }
# Change work directory accordingly
    chdir ($All{"workPath"});

# Cleanup from old files
    if (-e "ausgabe1.inp") {
	system("rm -f ausgabe*");
    }
    
# Data collection arrays
    my @rgedata = ();
    my @seqdata = ();
    my @lines = ();
    my $Progress=0;
    $All{"SdzFlag"}=0;
    if ($All{"scanSeq"} eq "checked") {
# For multiple runs or a scan
	for (my $Val=$All{"scanFrom"};$Val<=$All{"scanTo"};$Val=$Val+$All{"scanStep"}) {
	    @SValues=(@SValues,$Val);
	}
	
	my $ScanName = "";
	my $ScanVar = "";
	if ($All{"comboScan"}==0) {
	    $ScanName = "E";
	    $ScanVar = "valEnergy";
	} elsif ($All{"comboScan"}==1) {
	    $ScanName = "SigE";
	    $ScanVar = "sigEnergy";
	} elsif ($All{"comboScan"}==2) {
	    $ScanName = "Angle";
	    $ScanVar = "valAngle";
	} elsif ($All{"comboScan"}==3) {
	    $ScanName = "SigAngle";
	    $ScanVar = "sigAngle";
	} elsif ($All{"comboScan"}==4) {
	    $ScanName = "N";
	    $ScanVar = "numberProj";
	} elsif ($All{"comboScan"}==5) { 
	    $ScanName = "Ld".$All{"scandL"};
	    $ScanVar = "layer".$All{"scandL"}."d";
	} 
	
	my $iScan=0;
	my $FILENAME="";

#	my $FILENAME="DODO_".$ScanName.$_;
	foreach (@SValues) {
	    $All{$ScanVar} = $_;
	    # Here we are supposed to change variable on GUI to keep track
	    # I still do not know how to do that
	    if ( $All{"SdzFlag"} == 1) {
		if ($All{"comboScan"}==0) {
#		    this->{ui}->dz->setText($SdzValues[$iScan]);
		} elsif ($All{"comboScan"}==3) {
#		    this->valEnergy->setText($SdzValues[$iScan]);
		}
	    }
	    my $eingabe1=CreateInpFile(\%All);
	    if ($eingabe1 eq "ERROR") {return 0;}
	    $FILENAME=$All{"fileNamePrefix"}."_".$ScanName.$_;
	    open (INPF,q{>}, "$FILENAME.inp" );
	    print INPF $eingabe1;
	    close(INPF);
# Use Linux version	    
	    $Progress=$Progress+90/$#SValues;
#	    this->{ui}->progress->setValue($Progress);
#	    this->{ui}->progress->update();
	    $cmd = "cp $FILENAME.inp eingabe1.inp; $trimbin >> $All{'fileNamePrefix'}.log 2>&1";
	    system($cmd);
	    
	    foreach ("err","out","rge") {
		system("mv -f ausgabe1.$_ $FILENAME.$_ ");
	    }

# Collect data for plotting
	    open (RGE,q{<},"$FILENAME.rge" );
	    @lines = <RGE>;
	    close(RGE);
# Remove empty lines
	    @lines = grep {/\S/} @lines;
# Remove label line
	    shift @lines;
# Remove end of line
	    chomp @lines;
	    @rgedata=(@rgedata, [@lines]);

	    $iScan++; 
	}
    } else {
# For a single run
	my $eingabe1=CreateInpFile(\%All);
	if ($eingabe1 eq "ERROR") {return 0;}
	my $FILENAME=$All{"fileNamePrefix"};
	open (INPF,q{>}, "$FILENAME.inp" );
	print INPF $eingabe1;
	close(INPF);
	$Progress=20;
#	this->{ui}->progress->setValue($Progress);

# Use Linux versino
	$cmd = "cp $FILENAME.inp eingabe1.inp; $trimbin >> $All{'fileNamePrefix'}.log 2>&1";
	system($cmd);
	foreach ("err","out","rge") {
	    system("mv -f ausgabe1.$_ $FILENAME.$_ ");
	}

# Collect data for plotting
	open (RGE,q{<},"$FILENAME.rge" );
	@lines = <RGE>;
	close(RGE);
# Remove empty lines
	@lines = grep {/\S/} @lines;
# Remove label line
	shift @lines;
	@rgedata=(@rgedata, [@lines]);

	$Progress=90;
#	this->{ui}->progress->setValue($Progress);
    }
# Move the fort.33 file into the subdirectory and change its name
    $cmd="rm -f eingabe1.inp; mv -f fort.33 $All{'fileNamePrefix'}_Seq_Results.dat ";
    system($cmd);
# Archive and clean
    $cmd="tar cvzf /var/www/html/tmp/$All{'fileNamePrefix'}.tgz * >> /dev/null; cd ..; rm -rf $All{'workPath'}";
    system($cmd);
    $Progress=100;
#    this->{ui}->progress->setValue($Progress);
   return(\@rgedata,\@seqdata);
}	

# Function: Create and return input file for the Trim.SP simulation
# binary
sub CreateInpFile 
{
    use lib "/var/www/cgi-bin";
    use Chem;

# Get values from input
    my %All = %{$_[0]};

# Chemical formulas will be parsed on the fly for each layer. However,
# we will check if all the layers have inputs for composition,
# thickness and density. If not fail and crash :)
    
# Values of Z,A as well as other needed parameters are obtained from
# Chem.pm.
    
# This is the form of the begining of the input file:
    my $TemplateFile=
	    " ProjZ ProjAM valEnergy sigEnergy valAngle sigAngle parEF parESB parSHEATH parERC
 numberProj ranSeed ranSeed ranSeed z0 parRD dz parCA parKK0 parKK0R parKDEE1 parKDEE2 parIPOT parIPOTR parIRL";
 
# Then comes the number of layers (new format) for example 4 layers:
#  N_Layers=4
    $TemplateFile=$TemplateFile."\n"."N_Layers=numLayer"."\n";
    			 
# Then loop over the layers and for each give the following structure
   my $TemplateLayer=
 "L1d L1rho L1CK
 L1ELZ1 L1ELZ2 L1ELZ3 L1ELZ4 L1ELZ5
 L1ELW1 L1ELW2 L1ELW3 L1ELW4 L1ELW5
 L1ELC1 L1ELC2 L1ELC3 L1ELC4 L1ELC5
 L1ELE1 L1ELE2 L1ELE3 L1ELE4 L1ELE5
 L10301 L10302 L10303 L10304 L10305
   0.0000   0.0000   0.0000   0.0000   0.0000
 L1ELST11 L1ELST21 L1ELST31 L1ELST41 L1ELST51
 L1ELST12 L1ELST22 L1ELST32 L1ELST42 L1ELST52
 L1ELST13 L1ELST23 L1ELST33 L1ELST43 L1ELST53
 L1ELST14 L1ELST24 L1ELST34 L1ELST44 L1ELST54
 L1ELST15 L1ELST25 L1ELST35 L1ELST45 L1ELST55
 ";

    my $projectile=$All{"ProjType"};
    $All{"ProjZ"}=sprintf("%6.2f",Chem::Zof($projectile));    
    $All{"ProjAM"}=sprintf("%6.2f",Chem::Massof($projectile));

# This is the flag for checking layers    
    my $Check=0;
    my $Layer="";
    my $Li="";
# Loop over layers an create appropriate values
    for (my $i=1;$i<=$All{"numLayer"};$i++){
	$Li = "L".$i;
	$Layer = $TemplateLayer;
	$Layer =~ s/L1/$Li/g;
	$TemplateFile=$TemplateFile.$Layer;
	$Check=0;
# Composition of layers	
	my $LComp="L".$i."Comp";
	my $Comp = $All{$LComp};
	$All{$LComp} = $Comp;
	my %LElComp=Chem::parse_formula($Comp);
	foreach my $key (keys %LElComp) {
# Check if composition is understood
	    if ($key eq "" || Chem::Zof($key) eq "") {
		$Check++;
	    }  
	}
	if ($Comp eq "") {$Check++;}
# Write composition to results file header
	

# Densities of layers
	my $Lrho="L".$i."rho";
	my $rho =  $All{$Lrho};
	$All{$Lrho}=sprintf("%6.2f",$rho);
	if ($rho eq "") {$Check++;}
	
# Thickness of layers
	my $Ld ="L".$i."d";
	my $d = $All{$Ld};
	$All{$Ld}=sprintf("%8.2f",$d);
	if ($d eq "") {$Check++;}
	
# Sanity check, is the layer supposed to have value? are they all there?
	if ($Check!=0) {
	    my $ErrMsg="Error: Bad chemical formula in Layer $i.\nPleach check!\n";
	    print STDERR $ErrMsg;
	    return("ERROR");
	}
	
	my $tmp = "L".$i."CK";
	$All{$tmp}=sprintf("%6.2f",1.0);
	
	my $Sum = 0;
	foreach (keys %LElComp) { 
	    $Sum=$Sum+$LElComp{$_};
	}
	if ($Sum==0) {$Sum=1;}
		
	my @Els = keys %LElComp;

	for (my $NEl=1;$NEl<=5;$NEl++) {
	    my $El = "";
	    if ($#Els >= $NEl-1) {
		$El = $Els[$NEl-1];
	    }
	    my $LEkey = "L".$i."EL";
	    my $ElZ = Chem::Zof($El);
	    my $ElW = Chem::Massof($El);
	    my $ElE = Chem::Elastof($El);
	    my $ElC = 0;
	    my $El030 = 30;
	    if ($El) {
		$ElC = $LElComp{$El}/$Sum;
	    } else {
		$El030 = 0.0;
	    }
	    
	    $All{$LEkey."Z".$NEl}=sprintf("%8.4f",$ElZ);
	    $All{$LEkey."W".$NEl}=sprintf("%8.4f",$ElW);
	    $All{$LEkey."C".$NEl}=sprintf("%8.4f",$ElC);
	    $All{$LEkey."E".$NEl}=sprintf("%8.4f",$ElE);
	    $All{"L".$i."030".$NEl}=sprintf("%8.4f",$El030);
	    
	    my $ElST = Chem::Stopicru($El);
	    my @ElSTs = split (/,/,$ElST);
	    my $j=1;
	    foreach (@ElSTs) {
		$LEkey = "L".$i."ELST".$NEl.$j;
		$j++;
		$All{$LEkey}=sprintf("%11.6f",$_);
	    }
	}
    }
    
    foreach my $key (keys %All) {
	if ($All{$key} ne ""){
	    $TemplateFile =~ s/$key/$All{$key}/;
	    # Seed repeats three times
	    if ($key eq "ranSeed") { $TemplateFile =~ s/$key/$All{$key}/g;}
	}
    }
    return($TemplateFile);
}


#######################################################################
# This function displays the GUI
sub SpitGUI {
# Feed in all parameters from the GUI
    my %All = %{$_[0]};

    %ProjSelect = ("Muon", "",
		   "Li8", "",
		   "B12", "",
		   "He", "",
		   "H", "");
    my $Projectile = $All{"ProjType"};
    $ProjSelect{$Projectile} = "selected=\"selected\"";

    print ("Content-type: text/html \n\n");
#    my $tmp = $All{"numLayer"};
#    foreach my $k (keys %All) {$TC=$TC."<BR><BR>".$k."==".$All{$k};}
#    $TC=$TC."<BR>There are $#All in hash";
 
#    if ($#All > 0) { # There is some input 
    my $html= qq{
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
 <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
 <!meta http-equiv="Refresh" content="60">
 <title>TrimSP GUI</title>
 <meta name="author" content="Zaher Salman"/>
  <style type="text/css">

/*----- Tabs -----*/
.tabs {
    width:100%;
    display:inline-block;
}
 
/*----- Tab Links -----*/
/* Clearfix */
.tab-links:after {
    display:block;
    clear:both;
    content:'';
}
 
.tab-links li {
    margin:0px 5px;
    float:left;
    list-style:none;
}
 
.tab-links a {
    font-size: 120%;
    font-weight: bold;
    background-color:#3498db; 
    color:#EEEEEE; 
    padding:9px 15px;
    display:inline-block;
    border-radius:3px 3px 0px 0px;
    color:#4c4c4c;
    transition:all linear 0.15s;
}
 
.tab-links a:hover {
    background:#a7cce5;
    text-decoration:none;
}
 
li.active a, li.active a:hover {
    background:#ffffff;
    color:#4c4c4c;
}
 
/*----- Content of Tabs -----*/
.tab-content {
    padding:15px;
    border-radius:3px;
    box-shadow:-1px 1px 1px rgba(0,0,0,0.15);
    background:#fff;
}
 
.tab {
    display:none;
}
 
.tab.active {
    display:block;
}

</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});

function getFiles(dir, filelist){
    fileList = [];
    var fs = require('fs'); 
    var files = fs.readdirSync(dir);
    for(var i in files){
        if (!files.hasOwnProperty(i)) continue;
        var name = dir+'/'+files[i];
        if (!fs.statSync(name).isDirectory()){
            fileList.push(name);
        }
    }
    return fileList;
}

function rho_fun()
{
  var irow = \$(this).closest("tr").index();
  var caller = this.id;
  var chem = document.getElementById(caller).value;
  var rhoLi = "L"+irow+"rho";
  var hashPetName = new Object();

  var rhos = {
                     "NbN":8.47,
                     "Bi2Se3":7.51,
                     "La2CuO4": 7.08,
                     "La1.84Sr0.16CuO4": 6.94,
//                     "LSCO": 6.94,
                     "N2":1.145,
                     "ZnO":5.61,
                     "ZnSe":5.26,
                     "ZnS":4.09,
                     "ZrO":6.0,
                     "Gd3Ga5O12":7.08,
                     "MgAl2O4":3.60,
                     "NdGaO3":7.57,
                     "YAlO3":4.88,
                     "Y3Al5O12":4.55,
                     "LiF":2.60,
                     "CaF2":3.18,
                     "BaFe":4.83,
                     "MgF2":3.18,
                     "SiO2":2.65,
                     "TiO2":4.26,
                     "KTaO3":6.967,
                     "LaAlO3":6.70,
                     "Al2O3":3.98,
                     "SrTiO3":5.12,
                     "SrLaGaO4":6.389,
                     "SrLaAlO4":5.924,
                     "NbSe2":6.30,
                     "MgO":3.58,
                     "YBa2Cu3O7":6.54,
//                     "YBCO":6.54,
                     "GaAs":5.32,
                     "Mn12-Acetate":1.65,
                     "C60":1.65,
                     "H":0.08,
                     "He":0.12,
                     "Li":0.53,
                     "Be":1.85,
                     "B":2.34,
                     "C":2.26,
                     "N":1.03,
                     "O":2.00,
                     "F":1.11,
                     "Ne":1.50,
                     "Na":0.97,
                     "Mg":1.74,
                     "Al":2.7,
                     "Si":2.33,
                     "P":1.00,
                     "S":2.07,
                     "Cl":2.03,
                     "Ar":1.77,
                     "K":0.86,
                     "Ca":1.55,
                     "Sc":2.99,
                     "Ti":4.54,
                     "V":6.11,
                     "Cr":7.19,
                     "Mn":7.43,
                     "Fe":7.87,
                     "Co":8.9,
                     "Ni":8.9,
                     "Cu":8.96,
                     "Zn":7.13,
                     "Ga":5.91,
                     "Ge":5.32,
                     "As":5.72,
                     "Se":4.79,
                     "Br":3.14,
                     "Kr":3.10,
                     "Rb":1.53,
                     "Sr":2.54,
                     "Y":4.47,
                     "Zr":6.51,
                     "Nb":8.57,
                     "Mo":10.22,
                     "Tc":11.5,
                     "Ru":12.37,
                     "Rh":12.41,
                     "Pd":12.02,
                     "Ag":10.5,
                     "Cd":8.65,
                     "In":7.31,
                     "Sn":7.31,
                     "Sb":6.68,
                     "Te":6.24,
                     "I":4.93,
                     "Xe":3.80,
                     "Cs":1.90,
                     "Ba":3.59,
                     "La":6.15,
                     "Ce":6.77,
                     "Pr":6.77,
                     "Nd":7.01,
                     "Pm":7.22,
                     "Sm":7.52,
                     "Eu":5.24,
                     "Gd":7.9,
                     "Tb":8.23,
                     "Dy":8.55,
                     "Ho":8.8,
                     "Er":9.07,
                     "Tm":9.32,
                     "Yb":6.9,
                     "Lu":9.84,
                     "Hf":13.31,
                     "Ta":16.65,
                     "W":19.35,
                     "Re":21.04,
                     "Os":22.6,
                     "Ir":22.4,
                     "Pt":21.45,
                     "Au":19.32,
                     "Hg":13.55,
                     "Tl":11.85,
                     "Pb":11.35,
                     "Bi":9.75,
                     "Po":9.3,
                     "Th":11.72,
                     "Pa":15.4,
                     "U":18.95,
                     "":""
  }
  document.getElementById(rhoLi).value = rhos[chem];
}

function adjust_table()
{
   var numLayer = document.getElementById("numLayer").value;
   var LTable = document.getElementById("LTable");
   var Nrows = LTable.rows.length;

   var Comps = [$All{"Comps"}];
   var Rhos = [$All{"Rhos"}];
   var Ds = [$All{"Ds"}];

// Need to loop to get the right number of rows when reloading
   if (numLayer >= Nrows) {
      for (i = Nrows;i<=numLayer;i++) {
          var row = LTable.insertRow(i);
          var Li = row.insertCell(0);
          Li.innerHTML = i;
          var compL = row.insertCell(1);
          var compCell = document.createElement("input");
          compCell.id = "L"+i+"Comp";
          compCell.name = "L"+i+"Comp";
	  compCell.onchange = rho_fun;
	  if (Comps[i-1]) {compCell.value = Comps[i-1];}else{compCell.value = 'SrTiO3';}
          compL.appendChild(compCell);
          var rhoL = row.insertCell(2);
          var rhoCell = document.createElement("input");
          rhoCell.id = "L"+i+"rho";
          rhoCell.name = "L"+i+"rho";
          rhoL.appendChild(rhoCell);
	  if (Rhos[i-1]) {rhoCell.value = Rhos[i-1];}else{rhoCell.value = '5.12';}
          var thickL = row.insertCell(3);
          var thickCell = document.createElement("input");
          thickCell.id = "L"+i+"d";
          thickCell.name = "L"+i+"d";
          thickL.appendChild(thickCell);
	  if (Ds[i-1]) {thickCell.value = Ds[i-1];}else{thickCell.value = '10000';}
       }
    } else {
      for (i = Nrows-1;i>numLayer;i--) {
          LTable.deleteRow(i);
      }
    }
}

function adjust_scans()
{
// Also deal with scan checkbox
    var scanSeq = document.getElementById("scanSeq").checked;
    var sender = document.getElementById("scanSeq");
    ShowHide(sender,'Scans');
}

function ProjSmartDefaults()
{
   var Proj = document.getElementById("ProjType").value;
   var sigEnergy = document.getElementById("sigEnergy");
   var sigAngle = document.getElementById("sigAngle");

   if (Proj == "Muon") {
      sigEnergy.value = "450";
      sigAngle.value = "15";
   } else {
      sigEnergy.value = "0";
      sigAngle.value = "0";
   }      
}

function ProjNumberLimit()
{
   var numberProj = document.getElementById("numberProj").value;

   if (numberProj > 10000) {
       alert("Maximum number is 5000");
       document.getElementById("numberProj").value = 5000;
   }      
}


function ShowHide(sender,item) {
    var chbox = sender.id;
    if(sender.checked){
//       document.getElementById(item).style.display = 'block';
       document.getElementById(item).style.visibility = "visible";
    } else {
//       document.getElementById(item).style.display = 'none';
       document.getElementById(item).style.visibility = "hidden";
    }
}

function start_calc() {
// This function collects various values and launches the trim calculation
  var TrimSPcfg = "";
  var strtmp = "";
  var valtmp = "";

// Prepare Layers section
  var LayersSec = "[Layers],";
  var numLayer = document.getElementById("numLayer").value;
  LayersSec = LayersSec + "numLayer="+numLayer+",";
  for (i = 1;i<=numLayer;i++) {
    strtmp = "L"+i+"Comp";
    valtmp = document.getElementById(strtmp).value;
    LayersSec = LayersSec + strtmp +"="+ valtmp+",";
    strtmp = "L"+i+"rho";
    valtmp = document.getElementById(strtmp).value;
    LayersSec = LayersSec + strtmp +"="+ valtmp+",";
    strtmp = "L"+i+"d";
    valtmp = document.getElementById(strtmp).value;
    LayersSec = LayersSec + strtmp +"="+ valtmp+",";
  }

// Prepare ProjectileParameters section
  var ProjSec = "[ProjectileParameters],";
  strtmp = "ProjType";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "numberProj";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "z0";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "dz";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "valEnergy";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "sigEnergy";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "valAngle";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "sigAngle";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";
  strtmp = "ranSeed";
  valtmp = document.getElementById(strtmp).value;
  ProjSec = ProjSec + strtmp +"="+ valtmp+",";

// Prepare Files section
  var FilesSec = "[Files]";
  FilesSec = FilesSec + "FNPre=$All{""},Path=/tmp/webtmp/,";

// Prepare ScanSequence section
  var ScanSec = "[ScanSequence],";
  strtmp = "scanSeq";
  valtmp = 1*(document.getElementById(strtmp).checked);
  ScanSec = ScanSec + strtmp +"="+ valtmp+",";
  strtmp = "comboScan";
  valtmp = document.getElementById(strtmp).value;
  ScanSec = ScanSec + strtmp +"="+ valtmp+",";
  strtmp = "scanFrom";
  valtmp = document.getElementById(strtmp).value;
  ScanSec = ScanSec + strtmp +"="+ valtmp+",";
  strtmp = "scanStep";
  valtmp = document.getElementById(strtmp).value;
  ScanSec = ScanSec + strtmp +"="+ valtmp+",";
  strtmp = "scanTo";
  valtmp = document.getElementById(strtmp).value;
  ScanSec = ScanSec + strtmp +"="+ valtmp+",";


  TrimSPcfg = LayersSec + ProjSec + ScanSec;
//  document.location = "http://musruser.psi.ch/cgi-bin/TrimSP.cgi?TrimSPcfg="+TrimSPcfg+'go=start';
}

</script>

</head>
<body lang="en-CA" vlink="#800080" dir="ltr"  onload="adjust_table();adjust_scans()">
<form style="display: hidden" action="http://musruser.psi.ch/cgi-bin/TrimSP.cgi" method="GET">
<table cellpadding="4" cellspacing="0" border="2" >
<tr>
 <td colspan="5" width="100%" valign="top" border="1">
 <div class="tabs standard">
 <ul class="tab-links">
 <li class="active"><a href="#tab0">Layers</a></li>
 <li><a href="#tab1">Scans</a></li>
 <li><a href="#tab2">Plots</a></li>
 </ul>

 <div class="tab-content">
 <div id="tab0" class="tab active">
  <table border="1">
    <td style="vertical-align: top;"><table cellpadding="5">
	<tr><td><b>Layers</b></td></tr>
	<tr><td><label>Number of Layers: </label><input name="numLayer" id="numLayer" type="number" step="1" min="1" max="100" value="$All{'numLayer'}" onchange="adjust_table()">
	</td></tr>
	<tr><td>
	    <table id="LTable" border="2" cellpadding="10">
	      <tr><td><b>Layer #</b></td><td><b>Composition</b></td><td><b>Density [g/cm<sup>3</sup>]</b></td><td><b>Thickness [A]</b></td>
	    </table>
	</td></tr>
    </table></td>
    <td style="vertical-align: top;"><table cellpadding="5">
	<tr><td collspan="2"><b>Projectile parameters</b></td></tr>
	<tr><td>Projectile</td>
	  <td><select name="ProjType" id="ProjType" onchange="ProjSmartDefaults()" onload="ProjSmartDefaults()">
	      <option $ProjSelect{"Muon"} value="Muon">Muon</option>
	      <option $ProjSelect{"Li8"} value="Li8">Li8</option>
	      <option $ProjSelect{"He"} value="He">He</option>
	      <option $ProjSelect{"B12"} value="B12">B12</option>
	      <option $ProjSelect{"H"} value="H">H</option>
	    </select>
	  </td>
	</tr>
	<tr><td>Number of projectiles</td><td><input name="numberProj" id="numberProj" type="text" value="$All{"numberProj"}" onchange="ProjNumberLimit()"></td></tr>
	<tr><td>Starting depth [A]</td><td><input name="z0" id="z0" type="text" value="$All{"z0"}"></td></tr>
	<tr><td>Depth increment [A]</td><td><input name="dz" id="dz" type="text" value="$All{"dz"}"></td></tr>
	<tr><td>Energy [eV]</td><td><input name="valEnergy" id="valEnergy" type="text" value="$All{"valEnergy"}"></td></tr>
	<tr><td>Energy sigma [eV]</td><td><input name="sigEnergy" id="sigEnergy" type="text" value="$All{"sigEnergy"}"></td></tr>
	<tr><td>Angel [deg]</td><td><input name="valAngle" id="valAngle" type="text" value="$All{"valAngle"}"></td></tr>
	<tr><td>Angle sigma [deg]</td><td><input name="sigAngle" id="sigAngle" type="text" value="$All{"sigAngle"}"></td></tr>
	<tr><td>Random seed</td><td><input name="ranSeed" id="ranSeed" type="text" value="$All{"ranSeed"}"></td></tr>
	<tr><td><button type="submit" name="go" value="start" onclick="start_calc()">Start</button></td><td></td></tr> 
    </table></td></tr>
  </table>
 </div>
 <div id="tab1" class="tab">
   <input type="checkbox" name="scanSeq" id="scanSeq" onChange="ShowHide(this,'Scans')" $All{"scanSeq"} >Enable scans<br>
   <table id="Scans" style="visibility: hidden;">
     <tr><td>Scan parameter</td>
       <td>
	 <select name="comboScan" id="comboScan" onchange="">
	   <option value="EScan">Energy</option>
	   <option value="SigEScan">Energy Sigma</option>
	   <option value="AngleScan">Angle</option>
	   <option value="SigAngleScan">Angle Sigma</option>
	   <option value="NProjScan">Number of Projectiles</option>
	   <option value="dScan">Thickness of layer1</option>
	 </select>
       </td>
       <td></td>
     </tr>
     <tr>
       <td>
	 <!input type="radio" name="ScanType" value="Loop" checked onChange="">
	 <!input type="radio" name="ScanType" value="ScanList" onChange="">
	 From: <input name="scanFrom" id="scanFrom" type="text" value="$All{'scanFrom'}">
       </td>
       <td>
	 Step: <input name="scanStep" id="scanStep" type="text" value="$All{'scanStep'}">
       </td>
       <td>
	 To: <input name="scanTo" id="scanTo" type="text" value="$All{'scanTo'}">
       </td>
     </tr>
   </table>
 </div>
 <div id="tab2" class="tab">
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
     google.charts.load('45', {packages: ['corechart']});  
   </script>
</head>
<body>
<div id="container" style="width: 800px; height: 600px; margin: 0 auto"></div>
<script language="JavaScript">
function drawChart() {
   // Define the chart to be drawn.
   var data = new google.visualization.DataTable();
//   data.addColumn('number', 'Depth (nm)');
//   data.addColumn('number', 'Tokyo');
//   data.addColumn('number', 'New York');
//   data.addColumn('number', 'Berlin');
//   data.addColumn('number', 'London');
//   data.addRows([
//      [1,  7.0, -0.2, -0.9, 3.9],
//      [2,  6.9, 0.8, 0.6, 4.2],
//      [3,  9.5,  5.7, 3.5, 5.7],
//      [4,  14.5, 11.3, 8.4, 8.5],
//      [5,  18.2, 17.0, 13.5, 11.9],
//      [6,  21.5, 22.0, 17.0, 15.2],
//      [7,  25.2, 24.8, 18.6, 17.0],
//      [8,  26.5, 24.1, 17.9, 16.6],
//      [9,  23.3, 20.1, 14.3, 14.2],
//      [10,  18.3, 14.1, 9.0, 10.3],
//      [11,  13.9,  8.6, 3.9, 6.6],
//      [12,  9.6,  2.5,  1.0, 4.8]
//   ]);
       $All{'Rows'}


   // Set chart options
   var options = {'title' : '$All{'ProjType'} Stopping Profiles',
      hAxis: {
         title: 'Depth (nm)'
      },
      vAxis: {
         title: 'Stopped Particles'
      },   
      'width':800,
      'height':600,
      curveType: 'function',
      explorer: {
        actions: ['dragToZoom', 'rightClickToReset'],
        maxZoomOut:4,
        keepInBounds: true},
//	explorer.axis: both,
        pointsVisible: true	  
   };


   // Instantiate and draw the chart.
   var chart = new google.visualization.LineChart(document.getElementById('container'));
   chart.draw(data, options);
}
google.charts.setOnLoadCallback(drawChart);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<canvas id="myChart" width="50" height="50"></canvas>
<script>
/* var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['ZRed', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
*/
</script>
 </div>
 </div><!--end .main-->
</td>
</tr>
</table>
</form>
<p style="margin-bottom: 0in; line-height: 100%"><br/>
<script>//console.log('empty')</script>
</p>
$All{"links"}
<BR>$TC
</body>
</html>
};

print $html;
}

# This subroutine takes the results of the scan as input (2D array) and produces plot block for javascript
sub PlotFractions ()
{
    my ($frcdata_ref) = @_;
    my $Fracs="data.addColumn('number', 'Depth (nm)');\n";
    my @frcdata = @$frcdata_ref;
    my @SValues = @$SValues_ref;
    my @z = ();
    my $longest = 0;
    my $NFracs =  $#frcdata;
    my $NCols = 0;
    for (my $i=0;$i <= $NFracs; $i++) {
	$curren_len = $#{$frcdata[$i]};
	if ($curren_len > $NCols) {
	    $NCols = $curren_len;
	    @z=@{$frcdata[$i]};
	}
    }

    my @tmp1 = split(/\s+/,$z[1]);
    my @tmp2 = split(/\s+/,$z[2]);
    my $dz = 0.1*($tmp2[1]-$tmp1[1]);
    my $zval = 0;

    foreach (@SValues) {
	$Fracs = $Fracs."data.addColumn('number', 'E=$_ keV');\n";
    }
    $Fracs = $Fracs."data.addRows([\n";

    for (my $j=0; $j <= $NCols ; $j++) {
	$curren_len = 0;
	$zval = $dz*$j;
	$Fracs=$Fracs."[$zval, ";
	for (my $i=0;$i <= $NFracs; $i++) {
	    my @tmp = split(/\s+/,$frcdata[$i][$j]);
	    $Fracs=$Fracs.$tmp[2].",";
	    $curren_len++;
	}
	# find longest row
	$Fracs=$Fracs."],";
    }
    # Remove last comma
    $Fracs =~ s/,$//;

    $Fracs = $Fracs."]);\n";

#    $Fracs =~ s/\n/\\n/g;
    return($Fracs);
}
    
# This subroutine takes all rge data as input (2D array) and produces data block for javascript
sub PlotProfiles ()
{
    my ($rgedata_ref,$SValues_ref) = @_;
    my $Rows="data.addColumn('number', 'Depth (nm)');\n";
    my @rgedata = @$rgedata_ref;
    my @SValues = @$SValues_ref;
    my @z = ();
    my $longest = 0;
    my $NRows =  $#rgedata;
    my $NCols = 0;
    for (my $i=0;$i <= $NRows; $i++) {
	$curren_len = $#{$rgedata[$i]};
	if ($curren_len > $NCols) {
	    $NCols = $curren_len;
	    @z=@{$rgedata[$i]};
	}
    }

    my @tmp1 = split(/\s+/,$z[1]);
    my @tmp2 = split(/\s+/,$z[2]);
    my $dz = 0.1*($tmp2[1]-$tmp1[1]);
    my $zval = 0;

    foreach (@SValues) {
	$Rows = $Rows."data.addColumn('number', 'E=$_ keV');\n";
    }
    $Rows = $Rows."data.addRows([\n";

    for (my $j=0; $j <= $NCols ; $j++) {
	$curren_len = 0;
	$zval = $dz*$j;
	$Rows=$Rows."[$zval, ";
	for (my $i=0;$i <= $NRows; $i++) {
	    my @tmp = split(/\s+/,$rgedata[$i][$j]);
	    $Rows=$Rows.$tmp[2].",";
	    $curren_len++;
	}
	# find longest row
	$Rows=$Rows."],";
    }
    # Remove last comma
    $Rows =~ s/,$//;

    $Rows = $Rows."]);\n";

#    $Rows =~ s/\n/\\n/g;
    return($Rows);
}
    
